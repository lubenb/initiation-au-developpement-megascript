# Initiation au Développement MégaScript
---
Ce script permet d'effectuer tous les tests liés aux cours de Mr. Palaysi.

## Utilisation
Pour l'exécuter, vous devez:
- Avoir `curl` d'installé (`sudo apt install curl -y`)
- Savoir exécuter une commande dans un terminal

Pour lancer le script, faites :
```bash
curl https://gitlabinfo.iutmontp.univ-montp2.fr/lubenb/initiation-au-developpement-megascript/-/raw/master/ui.bash | bash
```

## Crédits
- [Luben Bastien](https://gitlabinfo.iutmontp.univ-montp2.fr/lubenb) pour les scripts principaux et l'interface.
- [Elslander Adrien](https://gitlabinfo.iutmontp.univ-montp2.fr/elslandera) pour les scripts complémentaires, l'intéraction client (orthographe et interface) et la lisibilité du code.
- [Di-Rollo Alix](https://gitlabinfo.iutmontp.univ-montp2.fr/dirolloa) et [Scoma-Geblin Gabriel](https://gitlabinfo.iutmontp.univ-montp2.fr/scomag) pour le rassemblement des tests du prof.