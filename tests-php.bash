#!/bin/bash

# Ce fichier n'est pas fait pour être executé seul, il doit être importé.

afficherLigneComplete () {
    longeurLigne=$(tput cols)
    # shellcheck disable=SC2004
    longeurLigne=$(($longeurLigne-1))
    x=1
    while [ $x -le "$longeurLigne" ]; do
        echo -n "-"
        # shellcheck disable=SC2004
        x=$(( $x + 1 ))
    done
    echo ""
}
echo "Ceci est du debug, n'y faites pas attention"

# Définition des tests

testFormatage () {
    printf "\n"
    echo "-- Scripts Formatage"
    printf "\n"

    echo "Téléchargement du fichier de test d'indentation"

    curl https://gitlabinfo.iutmontp.univ-montp2.fr/lubenb/initiation-au-developpement-megascript/-/raw/master/tools/indent-check.php > /tmp/initdevmegascript/indent-check.php 2> /dev/null

    echo "Téléchargement terminé, exécution sur tous les fichiers php"

    find . -iname "*.php" -exec bash -c "php /tmp/initdevmegascript/indent-check.php \"{}\"" \; | sort

    echo "-- Fin Scripts Formatage"
}

testTD1 () {
    printf "\n"
    echo "-- Scripts TD1"
    printf "\n"

    # find . -name script-division-entière.php -exec bash -c "grep -H Donnée {} | grep entiers | grep -e 'strictement positifs' -e '> 0' && echo {} OK || echo {} ÉCHEC" \; | sort
    # find . -name script-division-entière.php -exec bash -c "grep -H Donnée {} | grep entiers" \;
    # find . -name script-division-entière.php -exec bash -c "grep -q '* Donnée :' {} && grep -q '* Résultat :' {} && echo {} OK || echo {} ÉCHEC" \; | sort

    # find . -name script-somme-0.php -exec bash -c "php vérifier-indentation-de.php {}" \; | sort

    find . -name script-boucle-simple.php -exec bash -c "! grep -q -F ' . ' {} && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | grep ÉCHEC
    find . -name script-boucle-simple.php -exec bash -c 'echo -e "12\n" | php {} | grep -q "^13 14 15 16 17 18 19 20 21 22" && echo {} OK || echo {} ÉCHEC' \; | sort 2>/dev/null

    find . -name script-convertisseur-de-base.php -exec bash -c "echo -e '123\n4\n' | php {} | grep -q 1323 && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-convertisseur-de-base.php -exec bash -c "grep -F 'function' {} || grep -F ' . ' {} || grep -F ' .= ' {} || grep -e '\[.*\]' {} && echo {} ÉCHEC || echo {} OK" \; | sort
    find . -name script-convertisseur-de-base.php -exec bash -c "php -l {} >/dev/null && echo {} SYNTAXE OK  || echo {} SYNTAXE ÉCHEC" \; | sort
    find . -name script-convertisseur-de-base.php -exec bash -c "echo -e '9\n9\n' | timeout 1 php {} | grep -q '^10$' && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort

    find . -name script-division-entière.php -exec bash -c "echo -e '28\n7\n' | php {} > /tmp/initdevmegascript/resultattests/division.txt 2>/tmp/initdevmegascript/resultattests/erreursdivision.txt && ! grep -q Warning /tmp/initdevmegascript/resultattests/erreursdivision.txt && grep -q '28 = 7 x 4 + 0' /tmp/initdevmegascript/resultattests/division.txt && echo {} OK || echo \"{} ÉCHEC (mauvais résultat n°1)\"" \; | sort
    find . -name script-division-entière.php -exec bash -c "echo -e '30\n7\n' | php {} | grep -q '30 = 7 x 4 + 2' && ! grep 'if *(' {} && echo -e '28\n7\n' | php {} > /tmp/division.txt 2>/tmp/erreurs.txt && ! grep -q Warning /tmp/erreurs.txt && grep -q '28 = 7 x 4 + 0' /tmp/division.txt && phpcs --standard=PSR12 -n {} >/dev/null && grep -q '* Donnée :' {} && grep -q '* Résultat :' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-division-entière.php -exec bash -c "echo -e '30\n7\n' | php {} | grep -q '30 = 7 x 4 + 2' && echo {} OK || echo \"{} ÉCHEC (mauvais résultat n°2)\"" \; | sort
    find . -name script-division-entière.php -exec bash -c "grep 'if *(' {} && echo \"{} ÉCHEC (pas de if autorisé)\" || echo {} OK" \; | sort
    find . -name script-division-entière.php -exec bash -c "grep -H Donnée {} | grep entiers | grep -e 'strictement positifs' -e '> 0' && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name script-fibonacci.php -exec bash -c "! grep -q if {} && ! grep -q function {} && ! grep -q while {} && echo {} OK || echo \"{} ÉCHEC (pas de if ni de fonctions autorisés)\"" \; | sort
    find . -name script-fibonacci.php -exec bash -c "echo -e '1\n' | php {} 2>/dev/null | grep -q '###1###' && echo -e '2\n' | php {} | grep -q '###1###' && echo -e '3\n' | php {} | grep -q '###2###' && echo -e '7\n' | php {} 2>/dev/null | grep -q '###13###' && echo {} OK || echo \"{} ÉCHEC (mauvais résultat)\"" \; | sort
    find . -name script-fibonacci.php -exec bash -c "grep -l while {} && echo \"{} ÉCHEC (pas de while)\" || echo {} OK" \; | sort
    find . -name script-fibonacci.php -exec bash -c "grep -q '* Donnée :' {} && grep -q '* Résultat :' {} && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name script-moyenne-des-ages.php -exec bash -c "! grep -l 'if *(' {} && grep 'for (' {} && grep 'while (' {} && echo -e '4\n-33\n10\n20\n30\n0\n-11\n40\n' | php {} | grep 25 && echo -e '6\n1\n2\n3\n4\n5\n6\n' | timeout 1 php {} | grep -F '3.5' && echo -e '-4\n4\n12\n18\n19\n29\n' | timeout 1 php {} | grep -qe '19.5' && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | grep script | sort
    find . -name script-moyenne-des-ages.php -exec bash -c "echo -e '-4\n4\n12\n18\n19\n29\n' | timeout 1 php {} | grep -qe '19.5' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-moyenne-des-ages.php -exec bash -c "grep -q 'for (' {} && grep -q 'while (' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-moyenne-des-ages.php -exec bash -c "grep -q 'for (' {} && grep -q 'while (' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-moyenne-des-ages.php -exec bash -c "grep null {} && echo \"{} ÉCHEC (pas de null)\" || echo {} OK" \; | sort
    find . -name script-moyenne-des-ages.php -exec bash -c 'echo -e "4\n-33\n10\n20\n30\n0\n-11\n40\n" | php {} | grep 25 && echo {} OK || echo {} ÉCHEC' \; | sort
    find . -name script-moyenne-des-ages.php -exec bash -c 'echo -e "4\n10\n20\n30\n40\n" | php {} | grep 25 && echo {} OK || echo {} ÉCHEC' \; | grep script | sort
    find . -name script-moyenne-des-ages.php -exec bash -c 'echo -e "6\n1\n2\n3\n4\n5\n6\n" | timeout 1 php {} | grep -F "3.5" && echo {} OK || echo {} ÉCHEC' \; | sort
    find . -name script-moyenne-des-ages.php -exec bash -c 'grep "for (" {} && grep "while (" {} && echo -e "4\n-33\n10\n20\n30\n0\n-11\n40\n" | php {} | grep 25 && echo {} OK || echo {} ÉCHEC' \; 2>/dev/null | grep script | sort
    find . -name script-moyenne-des-ages.php -exec bash -c 'grep -l "if *(" {} && echo "{} ÉCHEC (pas de if autorisé)" || echo {} OK' \; | sort

    find . -name script-somme-0.php -exec bash -c "grep -q '^ \* Donnée :' {} && grep -q '^ \* Résultat :' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-somme-0.php -exec bash -c 'echo -e "-5\n5\n-6\n0\n" | php {} 2>/dev/null | grep -F "longueur=3 et somme=-6" && echo {} OK || echo "{} ÉCHEC (test n°1)"' \; | grep script | sort
    find . -name script-somme-0.php -exec bash -c 'echo -e "8\n6\n1\n2\n3\n0\n" | php {} 2>/dev/null | grep -F "longueur=5 et somme=20" && echo {} OK || echo "{} ÉCHEC (test n°2)"' \; | grep script | sort
    find . -name script-somme-0.php -exec bash -c 'grep -q -e if -e break {} && echo {} "ÉCHEC (pas de if ni de break autorisés)" || echo {} OK' \; | sort
    # find . -name script-somme-0.php -exec bash -c "grep -H Donnée {}" \; | sort
    # find . -name script-somme-0.php -exec bash -c "grep -H Donnée {} | grep -e nombres -e entiers" \; | sort
    # find . -name script-somme-0.php -exec bash -c "grep -H Donnée {} | grep -e nombres -e entiers | grep -e ' 0'" \; | sort

    find . -name script-somme-n.php -exec bash -c "echo -e '0\n' | php {} | grep -q '^0$' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-somme-n.php -exec bash -c "grep -q '* Donnée :' {} && grep -q '* Résultat :' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-somme-n.php -exec bash -c "grep -q 'if *(' {} && echo {} \"ÉCHEC (pas de if autorisé)\" || echo {} OK" \;| sort 
    find . -name script-somme-n.php -exec bash -c "grep -q for {} && ! grep -q while {} && echo {} OK || echo {} ÉCHEC \"(for préféré par rapport à while)\"" \;| sort
    find . -name script-somme-n.php -exec bash -c 'echo -e "5\n200\n99\n101\n20\n1\n" | php {} | grep 421 && echo {} OK || echo {} ÉCHEC' \; 2>/dev/null | grep script | sort
    find . -name script-somme-n.php -exec bash -c "echo -e '5\n200\n99\n101\n20\n1\n' | php {} | grep 421 && echo -e '0\n' | php {} | grep -q '^0$' && grep -q for {} && ! grep -q while {} && phpcs --standard=PSR12 '{}' >/dev/null && grep -q '* Donnée :' {} && grep -q '* Résultat :' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-somme-n.php -exec bash -c "grep -H Donnée {} | grep entier" \; | sort

    printf "\n"
    echo "-- Fin Scripts TD1"
    printf "\n"
}

testTD2 () {
    printf "\n"
    echo "-- Scripts TD2"
    printf "\n"

 	find . -name script-somme-la-plus-proche.php -exec bash -c "! grep -q 'for (' {} && echo {} OK || echo \"{} ÉCHEC (pas de for autorisé)\"" \; | sort

    # find . -name script-nombre-de-lettres-m.php -exec bash -c "cp {} out.php && phpcbf --standard=PSR12 out.php >/dev/null && grep -e '[[:alpha:]]\{1,\}(' out.php | grep -v fgetc | grep -v print | grep -v echo >/dev/null && echo {}" \;

    # find . -name script-nombre-de-mots.php -exec bash -c "phpcs --standard=PSR12 {} >/dev/null && php vérifier-indentation-de.php {} >/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name "script-longueur-dernier-mot.php" -exec bash -c "grep -E '^[^function].*[a-z]+\(.*\)' {} | grep -q -v -e 'fgetc' -e 'print' -e 'echo' && echo {} ÉCHEC || echo {} OK" \; | sort

    find . -name jeu-de-cartes.php -exec bash -c "echo -e '1\n1\n3\n3\n' | php {} | grep -q 'est une double paire' && echo -e '1\n2\n1\n2\n' | php {} | grep -q 'est une double paire' && echo -e '1\n2\n2\n1\n' | php {} | grep -q 'est une double paire' && echo {} OK || echo \"{} ÉCHEC (tesst n°4\"" \; | sort
    find . -name jeu-de-cartes.php -exec bash -c "echo -e '1\n2\n3\n4\n' | php {} | grep -q 'Pas de chance' && echo {} OK || echo \"{} ÉCHEC (test n°1)\"" \; | sort
    find . -name jeu-de-cartes.php -exec bash -c "echo -e '9\n9\n3\n4\n' | php {} | grep -q 'une paire' && echo -e '9\n 2\n 9\n 0\n' | php {} | grep -q 'une paire' && echo -e '9\n 2\n 3\n 9\n' | php {} | grep -q 'une paire' && echo -e '1\n 9\n 9\n 4\n' | php {} | grep -q 'une paire' && echo -e '1\n 9\n 3\n 9\n' | php {} | grep -q 'une paire' && echo -e '1\n 2\n 9\n 9\n' | php {} | grep -q 'une paire' && echo {} OK || echo \"{} ÉCHEC (test n°2)\"" \; | sort
    find . -name jeu-de-cartes.php -exec bash -c "echo -e '9\n9\n9\n0\n' | php {} | grep -q brelan && echo '9\n\n9\n0\n9\n' | php {} | grep -q brelan && echo '1\n0\n1\n1\n' | php {} | grep -q brelan && echo -e '0\n7\n7\n7\n' | php {} | grep -q brelan && echo {} OK || echo \"{} ÉCHEC (test n°3)\"" \; 2>/dev/null | sort
    find . -name jeu-de-cartes.php -exec bash -c "echo -e '9\n9\n9\n0\n' | php {} | grep -q brelan && echo '9\n\n9\n0\n9\n' | php {} | grep -q brelan && echo '1\n0\n1\n1\n' | php {} | grep -q brelan && echo -e '0\n7\n7\n7\n' | php {} | grep -q brelan && echo -e '9\n9\n3\n4\n' | php {} | grep -q 'une paire' && echo -e '9\n 2\n 9\n 0\n' | php {} | grep -q 'une paire' && echo -e '9\n 2\n 3\n 9\n' | php {} | grep -q 'une paire' && echo -e '1\n 9\n 9\n 4\n' | php {} | grep -q 'une paire' && echo -e '1\n 9\n 3\n 9\n' | php {} | grep -q 'une paire' && echo -e '1\n 2\n 9\n 9\n' | php {} | grep -q 'une paire' && echo -e '1\n2\n3\n4\n' | php {} >cartes.txt && grep -q 'Pas de chance' cartes.txt && echo -e '1\n1\n3\n3\n' | php {} | grep -q 'est une double paire' && echo -e '1\n2\n1\n2\n' | php {} | grep -q 'est une double paire' && echo -e '1\n2\n2\n1\n' | php {} | grep -q 'est une double paire' && grep -c 'elseif (' {} | grep -q 3 && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort

    find . -name script-année-bissextile.php -exec bash -c "echo -e '2000\n' | php {} | grep -q 'année bissextile' && echo -e '2023\n' | php {} | grep -q 'année non bissextile' && echo -e '2024\n' | php {} | grep -q 'année bissextile' && echo -e '2100\n' | php {} | grep -q 'année non bissextile' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-année-bissextile.php -exec bash -c "grep -c -e 'if *(' {} | grep 1 && grep -c 'else' {} | grep 1 && echo {} OK || echo \"{} ÉCHEC (un seul if et un seul else autorisés)\"" \; | sort
    find . -name script-année-bissextile.php -exec bash -c "grep -q '* Donnée :' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-année-bissextile.php -exec bash -c "grep -H '* Résultat :' {} | grep [Aa]ffiche | grep bissextile | grep non" \; | sort

    find . -name script-ax2bxc.php -exec bash -c "echo -e '1\n2\n1\n' | php {} 2>/dev/null | grep -q x=-1 && echo {} OK || echo \"{} ÉCHEC (test n°1)\"" \; | sort
    find . -name script-ax2bxc.php -exec bash -c "echo -e '1\n2\n3\n' | php {} | grep -q 'pas de solutions' && echo {} OK || echo \"{} ÉCHEC (test n°2)\"" \; | sort
    find . -name script-ax2bxc.php -exec bash -c "echo -e '2\n-5\n2\n' | php {} 2>/dev/null | grep -E 'x1=0.5 et x2=2|x1=2 et x2=0.5' && echo {} OK || echo \"{} ÉCHEC (test n°3)\"" \; | grep php | sort
    # find . -name script-ax2bxc.php -exec bash -c "grep -H '* Donnée :' {}" \; | sort
    find . -name script-ax2bxc.php -exec bash -c "grep -q 'if (' {} && grep -q 'elseif (' {} && grep -q 'else {' {} && echo {} OK || echo \"{} ÉCHEC (un if, un elif et un else seulement)\"" \; | sort
    # find . -name script-ax2bxc.php -exec bash -c "grep -H '* Donnée :' {} | grep -e nombres -e entiers -e coefficients | grep 'en entrée'" \; | sort

    find . -name script-longueur-dernier-mot.php -exec bash -c "echo 'Nous aimons la purée de pommes de terre.' | php {} | grep -qF '***5***' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-longueur-dernier-mot.php -exec bash -c "echo 'Verdoyant.' | php {} | grep -Fq '***9***' && echo {} OK || echo \"{} ÉCHEC (cas phrase avec 1 mot)\"" \; | sort

    find . -name script-longueur-plus-long-mot.php -exec bash -c "! grep -E '^[^function].*[a-z]+\(.*\)' {} | grep -v -E 'fgetc|print|echo' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-longueur-plus-long-mot.php -exec bash -c "echo 'Vendredi.' | php {} 2>/dev/null | grep -qF '***8***' && echo {} OK || echo \"{} ÉCHEC (cas phrase avec 1 mot)\"" \; | sort
    find . -name script-longueur-plus-long-mot.php -exec bash -c "echo 'We only said goodbye with words.' | php {} 2>/dev/null | grep -qF '***7***' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-longueur-plus-long-mot.php -exec bash -c "echo 'Deux et deux font.' | php {} 2>/dev/null | grep -qF '***4***' && echo {} OK || echo {} ÉCHEC" \; | sort | grep ÉCHEC

    find . -name script-nombre-de-le.php -exec bash -c "grep -c fgetc {} | grep -q 1 && echo {} OK || echo \"{} ÉCHEC (seul 1 appel a \`fgetc\` autorisé)\"" \; | sort
    find . -name script-nombre-de-le.php 2>/dev/null -exec bash -c "echo -e 'le.' | php {} | grep -Fq \"***1***\" && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-nombre-de-le.php 2>/dev/null -exec bash -c 'echo -e "Je lisais sous une tonnelle bleue le lundi." | php {} | grep -Fq "***3***" && echo {} OK || echo {} ÉCHEC' \; | sort
    find . -name script-nombre-de-le.php -exec bash -c "echo -e 'le.' | php {} 2>erreurs.txt | grep -Fq '***1***' && ! [ -s erreurs.txt ] && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort

    find . -name script-nombre-de-lettres-m.php -exec bash -c "echo -e 'Le maire de Commana travaille même le dimanche.' | timeout 1 php {} 2>/dev/null | grep -qF '***6***' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-nombre-de-lettres-m.php -exec bash -c "echo -e 'm.' | timeout 1 php {} 2>/dev/null | grep -qF '***1***' && echo {} OK || echo {} ÉCHEC" \; | sort
    # find . -name script-nombre-de-lettres-m.php -exec bash -c "grep -H Donnée {}" \; | sort
    find . -name script-nombre-de-lettres-m.php -exec bash -c "grep -H Donnée {} | grep entrée | grep -q point && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name script-nombre-de-mots.php -exec bash -c "grep -H Donnée {} | grep 'en entrée' | grep 'point'" \; | sort
    # find . -name script-nombre-de-mots.php -exec bash -c "grep -H Résultat {}" \;
    # find . -name script-nombre-de-mots.php -exec bash -c "grep -c fgetc {} | grep -q 1 && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name script-nombre-de-valeurs-paires.php -exec bash -c "! grep 'break;' {} && echo {} OK || echo \"{} ÉCHEC (pas de break autorisé)\"" \; | sort
    find . -name script-nombre-de-valeurs-paires.php -exec bash -c "echo -e '0\n0\n' | php {} | grep -qE '(^|\s)1(\s|$)' && echo {} OK || echo \"{} ÉCHEC (cas spécial avec 2 0)\"" \; 2>/dev/null | sort
    find . -name script-nombre-de-valeurs-paires.php -exec bash -c "grep -c 'if (' {} | grep -q 1 && echo {} OK || echo \"{} ÉCHEC (pas de if autorisé)\"" \; | sort
    find . -name script-nombre-de-valeurs-paires.php -exec timeout 1 bash -c "echo -e '1\n2\n3\n4\n5\n6\n50\n7\n8\n9\n0\n' | php {} | grep -qE '(^|\s)6(\s|$)' && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    # find . -name script-nombre-de-valeurs-paires.php -exec bash -c "grep -H Donnée {} | grep -e entiers -e nombres" \; | sort

    find . -name script-somme-la-plus-proche.php -exec timeout 1 bash -c 'echo -e "14\n" | php {} 2>/dev/null | grep -q "###15###" && echo {} OK || echo "{} ÉCHEC (test n°1)"' \; | sort
    find . -name script-somme-la-plus-proche.php -exec timeout 1 bash -c 'echo -e "32\n" | php {} 2>/dev/null | grep -q "###28###" && echo {} OK || echo "{} ÉCHEC (test n°2)"' \; | sort
    find . -name script-somme-la-plus-proche.php -exec bash -c "grep -c while {} | grep -q 1 && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name script-suite-croissante.php -exec bash -c "echo -e '10\n -11\n 2\n 3\n 4\n 5\n 6\n 7\n 8\n 6\n 10\n' | timeout 1 php {} >sortie.txt && grep -q faux sortie.txt && ! grep -q vrai sortie.txt && echo {} OK || echo \"{} ÉCHEC (test n°1)\"" \; | sort
    find . -name script-suite-croissante.php -exec bash -c "echo -e '8\n -11\n 2\n 3\n 4\n 5\n 6\n 7\n 8\n' | php {} >sortie.txt && grep -q vrai sortie.txt && ! grep -q faux sortie.txt && echo -e '10\n -11\n 2\n 3\n 4\n 5\n 6\n 7\n 8\n 6\n 10\n' | php {} >sortie.txt && grep -q faux sortie.txt && ! grep -q vrai sortie.txt && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-suite-croissante.php -exec bash -c "echo -e '8\n -11\n 2\n 3\n 4\n 5\n 6\n 7\n 8\n' | timeout 1 php {} >sortie.txt && grep -q vrai sortie.txt && ! grep -q faux sortie.txt && echo {} OK || echo \"{} ÉCHEC (test n°2)\"" \; | sort
    find . -name script-suite-croissante.php -exec bash -c "grep -q 'while (.* \(&&\|and\) .*)' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-suite-croissante.php -exec bash -c "rm sortie.txt" \;
    # find . -name script-suite-croissante.php -exec bash -c "grep -H 'while (.* \(&&\|and\) .*)' {}" \; | sort

    find . -name script-valeur-maximum.php -exec bash -c "echo -e '-1\n-2\n-3\n-4\n-5\n-6\n-7\n-8\n-9\n-10\n-11\n-12\n-13\n-14\n-15\n-16\n-17\n-18\n-19\n-20\n' | php {} | grep -e '-1' && echo {} OK || echo \"{} ÉCHEC (test n°1)\"" \; 2>/dev/null | sort
    find . -name script-valeur-maximum.php -exec bash -c "echo -e '-20\n-2\n-3\n-4\n-5\n-6\n-7\n-8\n-9\n-10\n-11\n-12\n-13\n-14\n-15\n-16\n-17\n-18\n-19\n-1\n' | php {} | grep -e '-1' && echo {} OK || echo \"{} ÉCHEC (test n°2)\"" \; 2>/dev/null | sort
    find . -name script-valeur-maximum.php -exec bash -c "echo -e '-20\n-2\n-3\n-4\n-5\n-6\n-7\n-8\n-9\n-10\n-11\n-12\n-13\n-14\n-15\n-16\n-17\n-18\n-19\n-1\n' | php {} | grep -qe '-1' && echo {} OK || echo \"{} ÉCHEC (test n°3)\"" \; 2>/dev/null | sort
    find . -name script-valeur-maximum.php -exec bash -c "echo -e '-9\n-2\n-3\n-4\n-5\n-6\n-7\n-8\n-1\n-10\n-11\n-12\n-13\n-14\n-15\n-16\n-17\n-18\n-19\n-20\n' | php {} | grep -e '-1' && echo {} OK || echo \"{} ÉCHEC (test n°4)\"" \; 2>/dev/null | sort
    find . -name script-valeur-maximum.php -exec bash -c "grep 'if (' {} | wc -l | grep -q 1 && ! grep -q -e else {} && echo {} OK || echo {} ÉCHEC" \; | sort | grep ÉCHEC
    # find . -name script-valeur-maximum.php -exec grep -H Donnée {} \; | sort
    # find . -name script-valeur-maximum.php -exec bash -c "grep -H Résultat {} | grep -e '[aA]ffiche' " \; | sort

    printf "\n"
    echo "-- Fin Scripts TD2"
    printf "\n"
}

testTD3 () {
    printf "\n"
    echo "-- Scripts TD3"
    printf "\n"

    # find . -name nombresAmis.php -exec bash -c "grep 'function sontAmis(int.*, int.*): bool' {} && echo {} OK || echo {} ÉCHEC" \; | sort

    # find . -name procédure-répéterCaractère.php -exec bash -c "grep -H '* Donnée :' {} | grep entier | grep caractère" \; | sort
    # find . -name procédure-répéterCaractère.php -exec bash -c "grep -H Résultat {} | grep -e '[aA]ffiche' -e écrit | grep -e caractère | grep -v nombre" \;| sort

    find . -name fonction-max2.php -exec bash -c "grep -l '* Donnée :' {}" \; | sort
    find . -name fonction-max2.php -exec bash -c "grep -q '* Donnée :' {} && grep -q '* Résultat :' {} && echo {} OK || echo \"{} ÉCHEC (Commentaire de description)\"" \;
    find . -name fonction-max2.php -exec bash -c "grep -q -e 'function max2(int .*, int .*): int' {} && echo {} OK || echo {} ÉCHEC" \; | sort |
    find . -name fonction-max2.php -exec bash -c "php -l {} >/dev/null && echo {} SYNTAXE OK  || echo {} SYNTAXE ÉCHEC" \; | sort
    find . -name fonction-max2.php -exec bash -c "php -r \"require('{}'); if (max2(10,12)==12 && max2(56,-12)==56 && max2(9,9)==9) {exit(0);} else {exit(1);}\" 2>/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name fonction-max2.php -exec bash -c "grep -H '* Résultat :' {} | grep -e [Rr]etourne -e [Rr]envoie" \; | sort

    find . -name fonction-max3.php -exec bash -c "grep -q 'function max3(int.*, int.*, int.*): int' {} && echo {} OK || echo \"{} ÉCHEC (Fonction mal définie)\"" \; | sort
    find . -name fonction-max3.php -exec bash -c "php -l {} >/dev/null && echo {} SYNTAXE OK || echo {} SYNTAXE ÉCHEC" \; | sort
    find . -name fonction-max3.php -exec bash -c "php -r \"require('{}'); if (max3(1,2,3)==3 && max3(1,3,2)==3 && max3(3,2,1)==3) {exit(0);} else {exit(1);}\" 2>/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name fonction-nombreDeTrianglesRectanglesDePérimètreAuPlus.php -exec bash -c "php -l {} >/dev/null && echo {} SYNTAXE OK  || echo {} SYNTAXE ÉCHEC" \; | sort
    find . -name fonction-nombreDeTrianglesRectanglesDePérimètreAuPlus.php -exec bash -c "timeout 1 php -r 'require(\"{}\"); nombreDeTrianglesRectanglesDePérimètreAuPlus(32) == 3 ? exit(0) : exit(1);' 1>/dev/null 2>/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name fonctions-nombre-de-chiffres.php -exec bash -c "php -r \"require('{}'); if (nbDeChiffresDe(987) == 3) {exit(0);} else {exit(1);}\" 2>/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name fonctions-nombre-de-chiffres.php -exec bash -c "timeout 1 php -r 'require(\"{}\"); nbDeChiffresDe(0) == 1 ? exit(0) : exit(1);' && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name fonctions-nombre-de-chiffres.php -exec bash -c "timeout 1 php -r 'require(\"{}\");nbDeChiffresDuCarréDe(10)==3 ? exit(0) : exit(1);' >/dev/null && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name "fonction-nombre*.php" -exec bash -c "grep -H -E '^[^function].*[a-z]+\(.*\)' {} && echo {} ÉCHEC || echo {} OK" \; | sort
    find . -name fonctions-nombre-de-chiffres.php -exec bash -c "timeout 1 php -r 'require(\"{}\");nbDeChiffresDuCarréDe(10)==3 ? exit(0) : exit(1);' >/dev/null && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort

    find . -name nombresAmis.php -exec bash -c "php -l {} >/dev/null && echo {} SYNTAXE OK  || echo {} SYNTAXE ÉCHEC" \; | sort
    find . -name nombresAmis.php -exec bash -c "timeout 1 php -r 'require(\"{}\");if (sontAmis(220,284) && sontAmis(110,142)==false) exit(0); else exit(1);' >/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name nombresAmis.php -exec bash -c "timeout 16 php -r 'require(\"{}\");afficherNombresAmisPlusPetitsQue(1211);' 1>/tmp/initdevmegascript/resultattests/td3sortie.txt 2>/dev/null && echo -e '(220,284)\n(1184,1210)' > /tmp/initdevmegascript/resultattests/td3cible.txt && cmp -s /tmp/initdevmegascript/resultattests/td3sortie.txt /tmp/initdevmegascript/resultattests/td3cible.txt && echo {} OK || echo \"{} ÉCHEC (un couple de nombres amis par ligne)\"" \; | sort
    find . -name nombresAmis.php -exec bash -c "/usr/bin/time -f '%e' php -r 'require(\"{}\");afficherNombresAmisPlusPetitsQue(1211);' 1>sortie.txt 2>err.txt && echo -e '(220,284)\n(1184,1210)' > cible.txt && cmp -s sortie.txt cible.txt && (cat err.txt | tr -d '\n' && echo {} OK) || echo {} ÉCHEC" \; | sort -n

    find . -name procédure-afficheNombresCroissants.php -exec bash -c "! grep -q while {} && echo {} OK || echo \"{} ÉCHEC (pas de while autorisé)\"" \;
    find . -name procédure-afficheNombresCroissants.php -exec bash -c "php -l {} >/dev/null && echo {} SYNTAXE OK  || echo {} SYNTAXE ÉCHEC" \; | sort
    find . -name procédure-afficheNombresCroissants.php -exec bash -c "php -r 'require(\"{}\");afficheNombresCroissants(-9,9);' 2>/dev/null | grep '5 4 3 2 1 0 1 2 3 4 5' && echo {} OK || echo {} ÉCHEC" \; | grep php | sort
    find . -name procédure-afficheNombresCroissants.php -exec bash -c "php -r 'require(\"{}\");afficheNombresCroissants(88,91);' 2>/dev/null | grep -qE '^8 9 0 1\s?$' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name procédure-afficheNombresCroissants.php -exec bash -c "php -r 'require(\"{}\");afficheNombresCroissants(-5,5);' 2>/dev/null | grep '5 4 3 2 1 0 1 2 3 4 5' && echo {} OK || echo {} ÉCHEC" \; | grep php | sort
    find . -name procédure-afficheNombresCroissants.php -exec bash -c "grep -H Donnée '{}' | grep -v ' c ' | grep -v 'entrée'" \; | sort

    find . -name procédure-afficheNombresDécroissants.php -exec bash -c "! grep -q while {} && php -r 'require(\"{}\");afficheNombresDécroissants(-13,-17);' 2>/dev/null | grep '^3 4 5 6 7$' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name procédure-afficheNombresDécroissants.php -exec bash -c "! grep -q while {} && php -r 'require(\"{}\");afficheNombresDécroissants(-13,-17);' 2>/dev/null | grep -q '^3 4 5 6 7$' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name procédure-afficheNombresDécroissants.php -exec bash -c "! grep while {} && echo {} OK || echo \"{} ÉCHEC (pas de while autorisé)\"" \;
    find . -name procédure-afficheNombresDécroissants.php -exec bash -c "php -l {} >/dev/null && echo {} SYNTAXE OK  || echo {} SYNTAXE ÉCHEC" \; | sort
    find . -name procédure-afficheNombresDécroissants.php -exec bash -c "php -r 'require(\"{}\");afficheNombresDécroissants(85,75);' 2>/dev/null | grep -qE '^5 4 3 2 1 0 9 8 7 6 5 *$' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name procédure-afficheNombresDécroissants.php -exec bash -c "! grep -q while {} && php -r 'require(\"{}\");afficheNombresDécroissants(-13,-17);' 2>/dev/null | grep -q '^3 4 5 6 7$' && php -r 'require(\"{}\");afficheNombresDécroissants(85,75);' 2>/dev/null | grep -qE '^5 4 3 2 1 0 9 8 7 6 5 *$' && grep -q Donnée {} && ! grep -q abs {} && grep Résultat {} | grep -q -e '[aA]ffiche' && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name procédure-affichePyramide.php -exec bash -c "! grep while {} && grep -c 'for (' {} | grep -q 1 && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name procédure-affichePyramide.php -exec bash -c "php -l {} >/dev/null && echo {} SYNTAXE OK  || echo {} SYNTAXE ÉCHEC" \; | sort
    find . -name procédure-affichePyramide.php -exec bash -c "php -r 'require(\"{}\"); affichePyramide(4);' 1>/tmp/initdevmegascript/resultattests/td3sortie.txt 2>/dev/null && echo -e '      1\n    2 3 2\n  3 4 5 4 3\n4 5 6 7 6 5 4' >/tmp/initdevmegascript/resultattests/td3cible.txt && cmp /tmp/initdevmegascript/resultattests/td3sortie.txt /tmp/initdevmegascript/resultattests/td3cible.txt >/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort
    # find . -name procédure-affichePyramide.php -exec grep -H 'Donnée' {} \; | sort

    find . -name procédure-pyramideSimple.php -exec bash -c "grep -q 'function pyramideSimple(int $.*, string $.*): void' {} && echo {} OK || echo {} ÉCHEC" \;
    find . -name procédure-pyramideSimple.php -exec bash -c "php -l {} >/dev/null && echo {} SYNTAXE OK  || echo {} SYNTAXE ÉCHEC" \; | sort
    find . -name procédure-pyramideSimple.php -exec bash -c "php -r 'require\"{}\";pyramideSimple(4,\"x\");' {} 2>/dev/null | grep -c -e '^xxxxxxx$' -e '^ xxxxx$' -e '^  xxx$' -e ' x$' | grep 4 && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name procédure-pyramideSimple.php -exec bash -c "grep -c 'répéterCaractère(' {} | grep -q 2 && echo {} OK || echo {} ÉCHEC" \; | sort

    printf "\n"
    echo "-- Fin Scripts TD3"
    printf "\n"
}

testTD4 () {
    printf "\n"
    echo "-- Scripts TD4"
    printf "\n"

    find . -name fonction-aChiffresTousDifférents.php -exec bash -c "! grep -q 'for (' {} && grep -q -e 'while (.* and .*)' -e 'while (.* && .*)' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name fonction-aChiffresTousDifférents.php -exec bash -c "php -l {} >/dev/null && echo {} SYNTAXE OK  || echo {} SYNTAXE ÉCHEC" \; | sort
    find . -name fonction-aChiffresTousDifférents.php -exec bash -c "php -r 'require(\"{}\"); aChiffresTousDifférents(13579)==true and aChiffresTousDifférents(24648)==false ? exit(0) : exit(1);' 1>/dev/null 2>/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort
    # find . -name fonction-aChiffresTousDifférents.php -exec bash -c "testeurs/TD4/extraire-boucles.php < {} | grep -q -e return -e break && echo {} ÉCHEC || echo {} OK" \; | grep ÉCHEC | sort

    find . -name fonction-fréquenceChiffres.php -exec bash -c "grep -E '^[^function].*[a-z]+\(.*\)' {} >/dev/null | grep -v -E 'array|intdiv' && echo {} ÉCHEC || echo {} OK" \; | sort
    find . -name fonction-fréquenceChiffres.php -exec bash -c "php -l {} 1>/dev/null 2>/dev/null && echo {} SYNTAXE OK || echo {} SYNTAXE ÉCHEC" \;
    find . -name fonction-fréquenceChiffres.php -exec bash -c "php -r 'require(\"{}\"); fréquenceChiffres(15121) == [0,3,1,0,0,1,0,0,0,0] ? exit(0) : exit(1);' 1>/dev/null 2>/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name fonction-fréquenceChiffres.php -exec bash -c "! grep -q for {} && grep -q while {} && echo {} OK || echo {} ÉCHEC" \; | sort

    # find . -name fonction-moyenne.php -exec bash -c "grep -H -e '* Donnée :' {} | grep tableau | grep -E '(entiers|nombres)' " \; | sort

    find . -name fonction-saisirTableauDEntiers.php -exec bash -c "echo -e '27\n3\n11\n4\n' | php -r 'require(\"{}\"); exit(saisirTableauDEntiers(4)==array(27,3,11,4)?0:1);' 2>/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name fonction-saisirTableauDEntiers.php -exec bash -c "php -r 'require(\"{}\"); exit(saisirTableauDEntiers(0)==array()?0:1);' 2>/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name fonction-saisirTableauDEntiers.php -exec bash -c "echo -e '27\n3\n11\n4\n' | php -r 'require(\"{}\"); exit(saisirTableauDEntiers(4)==array(27,3,11,4)?0:1);' 1>/dev/null 2>/dev/null && php -r 'require(\"{}\"); exit(saisirTableauDEntiers(0)==array()?0:1);' 1>/dev/null 2>/dev/null && phpcs --standard=PSR12 {} >/dev/null && php outils/vérifier-indentation-de.php {} >/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort | grep OK

    find . -name procédure-afficherTableauDEntiers.php -exec bash -c "timeout 1 php -r 'function terminer(){exit(1);} set_error_handler(\"terminer\"); require(\"{}\");afficherTableauDEntiers(array());' | grep -qF '[]' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name procédure-afficherTableauDEntiers.php -exec bash -c "timeout 1 php -r 'require(\"{}\");afficherTableauDEntiers(array(1,8,4,2,16));' 2>/dev/null | grep -F -q -e '[1,8,4,2,16]' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name procédure-afficherTableauDEntiers.php -exec bash -c "timeout 1 php -r 'require(\"{}\");afficherTableauDEntiers(array(1,8,4,2,16));' 2>/dev/null | grep -q -e '\(1,8,4,2,16\)' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name procédure-afficherTableauDEntiers.php -exec grep -H while {} \; | sort

    find . -name script-appartient-dans-tab.php -exec bash -c "echo -e '7\n1\n2\n3\n4\n55\n-6\n7\n8\n9\n100\n11\n12\n-13\n14\n15\n55\n' | php {} > out.txt && grep -qF '55 appartient à [7,1,2,3,4,55,-6,7,8,9,100,11,12,-13,14,15] ? vrai' out.txt && ! grep -q faux out.txt && rm out.txt && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-appartient-dans-tab.php -exec bash -c "echo -e '7\n1\n2\n3\n4\n55\n-6\n7\n8\n9\n100\n11\n12\n-13\n14\n15\n55\n' | php {} > out.txt && grep -qF '55 appartient à [7,1,2,3,4,55,-6,7,8,9,100,11,12,-13,14,15] ? vrai' out.txt && ! grep -q faux out.txt && rm out.txt && grep -c 'for (' {} | grep -q 2 && grep 'while (' {} | grep -q -E '&&|and' && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name script-appartient-dans-tab.php -exec bash -c "grep -c 'for (' {} | grep -q 2 && grep 'while (' {} | grep -q -E '&&|and' && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name script-produit-tab.php -exec bash -c "echo -e '0\n' | timeout 1 php {} 2>/dev/null | grep -qF 'Le produit des nombres de [] est égal à 1.' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-produit-tab.php -exec bash -c "echo -e '4\n 1\n 2\n 6\n 3\n' | php {} 2>/dev/null | grep -qF 'Le produit des nombres de [1,2,6,3] est égal à 36.' && echo {} OK || echo {} ÉCHEC" \;
    find . -name script-produit-tab.php -exec bash -c "grep -c 'for (' {} | grep -q 2 && grep -q 'while (' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-produit-tab.php -exec bash -c "php -l {} >/dev/null && echo {} SYNTAXE OK  || echo {} SYNTAXE ÉCHEC" \; | sort
    find . -name script-produit-tab.php -exec bash -c "echo -e '4\n 1\n 2\n 6\n 3\n' | timeout 1 php {} 2>/dev/null | grep -qF 'Le produit des nombres de [1,2,6,3] est égal à 36.' && echo -e '0\n' | timeout 1 php {} 2>/dev/null | grep -qF 'Le produit des nombres de [] est égal à 1.' && grep -c 'for (' {} | grep -q 2 && grep -q 'while (' {} && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name script-somme-tab.php -exec bash -c "echo -e '0\n' | timeout 1 php {} 2>/dev/null | grep -qF 'La somme des nombres de [] est égale à 0.' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-somme-tab.php -exec grep -l while {} \; | sort
    find . -name script-somme-tab.php -exec bash -c "grep -c 'for (' {} | grep -q 3 && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-somme-tab.php -exec bash -c "echo -e '4\n 1\n 2\n 6\n 3\n' | php {} 2>/dev/null | grep -qF 'La somme des nombres de [1,2,6,3] est égale à 12.' && echo -e '0\n' | timeout 1 php {} 2>/dev/null | grep -qF 'La somme des nombres de [] est égale à 0.' && ! grep -q while {} && grep -c for {} | grep -q 3 && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name script-traiterTableauDEntiers.php -exec bash -c "echo -e '1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n' | php {} 2>/dev/null | grep -F -e '[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]' -e '8.5' -e '[1,3,3,5,5,7,7,9,9,11,11,13,13,15,15,17]' | wc -l | grep -q 3 && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name script-traiterTableauDEntiers.php -exec bash -c "echo -e '1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n11\n12\n13\n14\n15\n16\n' | php {} 2>/dev/null | grep -F -e '[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]' -e '8.5' -e '[1,3,3,5,5,7,7,9,9,11,11,13,13,15,15,17]' | wc -l | grep -q 3 && phpcs --standard=PSR12 -n {} >/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort

    # find . -name fonction-estPalindrome.php -exec bash -c "php -d display_errors=1 -d error_reporting=E_ALL -r 'set_error_handler(function(\$errno, \$errstr, \$errfile, \$errline){fwrite(STDERR,\$errstr); exit(1); }); require(\"{}\"); (estPalindrome(\"X\") == true && estPalindrome(\"KAYAK\") == true && estPalindrome(\"RENVERSER\") == false && estPalindrome(\"XY\") == false) ? exit(0) : exit(1);' && echo {} OK || echo {} ÉCHEC " \; 2>/dev/null | sort
    find . -name fonction-estPalindrome.php -exec bash -c "! grep -q 'for (' {} && grep -q -e 'while (.* and .*)' -e 'while (.* && .*)' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name tests-fonction-estPalindrome.php -exec bash -c "grep -qE 'require' {} && grep -q 'estPalindrome(' {} && grep -qE 'echo|print|var_dump' {} && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name tests-fonction-estPalindrome.php -exec bash -c 'timeout 1 php "$0" >/dev/null 2>&1 || [ $? -eq 124 ] && echo "$0 OK" || echo "$0 ÉCHEC"' {} \; 2>/dev/null | sort
    # find . -name fonction-estPalindrome.php -exec bash -c "php outils/extraire-boucles.php < {} | (! grep -q -e break -e return) && echo {} OK || echo {} ÉCHEC" \; | sort | grep ÉCHEC

    printf "\n"
    echo "-- Fin Scripts TD4"
    printf "\n"
}

