#!/bin/bash

# Initiation au Développement - MégaScript

installPackagesPrincipaux () {
    if [[ $(grep -c "arch" /etc/os-release) -gt 0 ]]; then
        echo "Installation des packages nécessaires pour s'executer..."
        sudo pacman -S --noconfirm --needed dialog inotify-tools > /dev/null
    elif [[ $(grep -c "debian" /etc/os-release) -gt 0 ]]; then
        echo "Installation des packages nécessaires pour s'executer..."
        sudo apt-get install dialog inotify-tools -y > /dev/null
    else
        echo "Vous devez installer dialog et inotify-tools à la main"
        exit 1
    fi
}

checkPackagesPrincipaux () {
    if [[ $NOCHECK = 1 ]]; then
        echo "On skip les checks"
    elif [[ $(which dialog 2>&- | grep -qv ^/) ]]; then 
        installPackagesPrincipaux
    fi
}

installPhpCodeSniffer () {
    if [[ -f /etc/arch-release ]]; then
        echo "Vous devez installer php-codesniffer à la main"
        exit 1
    elif [[ -f /etc/debian-release ]]; then
        echo "Installation de php-codesniffer..."
        sudo apt-get install php-codesniffer -y
    else
        echo "Vous devez installer php-codesniffer à la main"
        exit 1
    fi
}

checkPhpCodeSniffer () {
    executeResult=$(which phpcs 2>&- | grep -c ^/)

    if [[ $executeResult  == 0 ]]; then
        if [[ $DEBUG == 1 ]]; then
            echo "[DEBUG] php-codesniffer non disponible, installation..."
        fi
        
    else
        if [[ $DEBUG == 1 ]]; then
            echo "[DEBUG] php-codesniffer disponible"
        fi
    fi
}

checkMiseAJour () {
    if [ -f /tmp/initdevmegascript/commit_hash.txt ]; then
        curl https://gitlabinfo.iutmontp.univ-montp2.fr/api/v4/projects/20066/repository/commits 2> /dev/null | python3 -c "import sys, json; print(json.load(sys.stdin)[0]['short_id'])" > /tmp/initdevmegascript/new_commit_hash.txt
        commithash=$(cat /tmp/initdevmegascript/new_commit_hash.txt)
        originalhash=$(cat /tmp/initdevmegascript/commit_hash.txt)
        if [[ $originalhash = "$commithash" ]]; then
            dialog --backtitle "Init. Au Dev. MegaScript" --infobox "Pas de mise à jour" 10 50
            sleep 1
        else
            dialog --backtitle "Init. Au Dev. MegaScript" --infobox "Mise à jour disponible, téléchargement..." 10 50
            
            curl https://gitlabinfo.iutmontp.univ-montp2.fr/lubenb/initiation-au-developpement-megascript/-/raw/master/tests-php.bash > /tmp/initdevmegascript/tests-php.bash 2> /dev/null
            curl https://gitlabinfo.iutmontp.univ-montp2.fr/lubenb/initiation-au-developpement-megascript/-/raw/master/tests-java.bash > /tmp/initdevmegascript/tests-java.bash 2> /dev/null
            curl https://gitlabinfo.iutmontp.univ-montp2.fr/lubenb/initiation-au-developpement-megascript/-/raw/master/tools/wait-tests.py > /tmp/initdevmegascript/wait-tests.py 2> /dev/null

            curl https://gitlabinfo.iutmontp.univ-montp2.fr/api/v4/projects/20066/repository/commits | python3 -c "import sys, json; print(json.load(sys.stdin)[0]['short_id'])" > /tmp/initdevmegascript/commit_hash.txt
        fi
    else
        dialog --backtitle "Init. Au Dev. MegaScript" --infobox "Téléchargement..." 10 50

        curl https://gitlabinfo.iutmontp.univ-montp2.fr/lubenb/initiation-au-developpement-megascript/-/raw/master/tests-php.bash > /tmp/initdevmegascript/tests-php.bash 2> /dev/null
        curl https://gitlabinfo.iutmontp.univ-montp2.fr/lubenb/initiation-au-developpement-megascript/-/raw/master/tests-java.bash > /tmp/initdevmegascript/tests-java.bash 2> /dev/null
        curl https://gitlabinfo.iutmontp.univ-montp2.fr/lubenb/initiation-au-developpement-megascript/-/raw/master/tools/wait-tests.py > /tmp/initdevmegascript/wait-tests.py 2> /dev/null

        curl https://gitlabinfo.iutmontp.univ-montp2.fr/api/v4/projects/20066/repository/commits 2> /dev/null | python3 -c "import sys, json; print(json.load(sys.stdin)[0]['short_id'])" > /tmp/initdevmegascript/commit_hash.txt
    fi

    
}

menuPrincipal () {
    # Vérification des tests
    dialog --backtitle "Init. Au Dev. MegaScript" --infobox "Vérification des mises à jour..." 10 50
    if [[ $DEBUG = 1 ]]; then
        echo "[DEBUG] Debug activé donc pas de mise à jour auto"
    elif [[ $STABLE = 1 ]]; then
        echo "Ce code n'est pas stable, qu'est ce que tu pensais ?"
        exit 1
    else
        checkMiseAJour
        telechargerTests
    fi
    
    # Importer les fichers de tests
    # shellcheck source=/dev/null
    source /tmp/initdevmegascript/tests-php.bash
    # shellcheck source=/dev/null
    source /tmp/initdevmegascript/tests-java.bash

    
    # Supprimer le fichier temporaire pour éviter que l'on garde l'ancien choix
    rm -f /tmp/initdevmegascript/menuselect.txt > /dev/null

    # MESSAGE DE BIENVENUE
    dialog --backtitle "Init. Au Dev. MegaScript" --msgbox "Bienvenue dans le MégaScript !\n\n\n\nVoici les dernières mises à jour :\n\n- Les tests JAVA ne sont pas 100% fonctionnels\n\n- Les tests du Jeu421 sont fonctionnels.\n\n- Il manque certains tests PHP, leur ajout est en cours...\n\n\n\nDernière mise à jour : 08/11 à 22h." 20 50

    # Demander ce que l'utilisateur veut faire
    dialog --backtitle "Init. Au Dev. MegaScript" --menu "Choisir une action:" 20 50 10\
        1 "Trouver les échecs" \
        2 "Exécuter tous les tests" \
        3 "Exécuter certains tests" \
        4 "Tester le style (PSR12)" \
        5 "Tester l'indentation" \
        6 "Formater tous les fichiers" \
    2> /tmp/initdevmegascript/menuselect.txt

    # Effacer tout ce que dialog a affiché
    clear

    # Lire ce que l'utilisateur a choisi
    optselected=$(cat /tmp/initdevmegascript/menuselect.txt)

    if [[ $DEBUG = 1 ]]; then
        echo "[DEBUG] Option selectionnée \"$optselected\""
    fi

    ## Ancienne méthode poru faire tout les tests + obtenir les échecs
    if [[ $optselected = 1 && $BETA = 1 ]]; then
    echo "Exécution de tous les tests..."
        rm -r /tmp/initdevmegascript/testsresult*
        rm -r /tmp/initdevmegascript/done
        touch /tmp/initdevmegascript/testsresult{1..4}.txt
        eval $(testTD1 >> /tmp/initdevmegascript/testsresult1.txt &)
        eval $(testTD2 >> /tmp/initdevmegascript/testsresult2.txt &)
        eval $(testTD3 >> /tmp/initdevmegascript/testsresult3.txt &)
        eval $(testTD4 >> /tmp/initdevmegascript/testsresult4.txt &)
        while sleep 1 ; do
            echo -n "."
            python3 /tmp/initdevmegascript/wait-tests.py
            if [[ -f /tmp/initdevmegascript/done ]]; then
                rm -f /tmp/initdevmegascript/done
                break
            fi
        done
        echo "Terminé !"
        rm -f /tmp/initdevmegascript/testsresult.txt
        touch /tmp/initdevmegascript/testsresult.txt
        # shellcheck disable=SC2061
        find /tmp/initdevmegascript -iname testsresult?.txt -exec bash -c "cat {} >> /tmp/initdevmegascript/testsresult.txt" \;
        cat /tmp/initdevmegascript/testsresult.txt | \grep -v -e "-- Fin" | \grep -e "--" -e "ÉCHEC"
        nbEchec=$(cat /tmp/initdevmegascript/testsresult.txt | grep -e "ÉCHEC" | wc -l)
        nbLn=$(cat /tmp/initdevmegascript/testsresult.txt | grep -e "ÉCHEC" -e "OK" | wc -l)
        echo ""
        echo "Vous avez $nbEchec erreur(s) sur $nbLn tests"

    elif [[ $optselected = 1 ]]; then
        
        echo "Exécution de tous les tests..."
        echo "" > /tmp/initdevmegascript/testsresult.txt
        { testTD1; testTD2; testTD3; testTD4; } >> /tmp/initdevmegascript/testsresult.txt
        cat /tmp/initdevmegascript/testsresult.txt | \grep -e "--" -e "ÉCHEC"
        nbEchec=$(cat /tmp/initdevmegascript/testsresult.txt | grep -e "ÉCHEC" | wc -l)
        nbLn=$(cat /tmp/initdevmegascript/testsresult.txt | wc -l)
        echo .
        echo "Vous avez $nbEchec erreur(s) sur $nbLn tests"
    elif [[ $optselected = 2 && $BETA = 1 ]]; then
        echo "Exécution de tous les tests..."
        rm -r /tmp/initdevmegascript/testsresult*
        touch /tmp/initdevmegascript/testsresult{1..4}.txt
        eval $(testTD1 >> /tmp/initdevmegascript/testsresult1.txt &)
        eval $(testTD2 >> /tmp/initdevmegascript/testsresult2.txt &)
        eval $(testTD3 >> /tmp/initdevmegascript/testsresult3.txt &)
        eval $(testTD4 >> /tmp/initdevmegascript/testsresult4.txt &)
        while sleep 1 ; do
            echo -n "."
            python3 /tmp/initdevmegascript/wait-tests.py
            if [[ -f /tmp/initdevmegascript/done ]]; then
                rm -f /tmp/initdevmegascript/done
                break
            fi
        done
        echo "Terminé !"
        rm -f /tmp/initdevmegascript/testsresult.txt
        touch /tmp/initdevmegascript/testsresult.txt
        # shellcheck disable=SC2061
        find /tmp/initdevmegascript -iname testsresult?.txt -exec bash -c "cat {} >> /tmp/initdevmegascript/testsresult.txt" \;
        cat /tmp/initdevmegascript/testsresult.txt | \grep -v -e "-- Fin"
        nbEchec=$(cat /tmp/initdevmegascript/testsresult.txt | grep -e "ÉCHEC" | wc -l)
        nbLn=$(cat /tmp/initdevmegascript/testsresult.txt | grep -e "ÉCHEC" -e "OK" | wc -l)
        echo ""
        echo "Vous avez $nbEchec erreur(s) sur $nbLn tests"
    elif [[ $optselected = 2 ]]; then
        echo "Exécution de tous les tests..."
        testTD1
        testTD2
        testTD3
        testTD4

    elif [[ $optselected = 3 ]]; then
        # Ouvrir le menu des choix des tests
        sousMenuChoixTests
    elif [[ $optselected = 4 ]]; then
        # Faire les tests de style PSR12
        checkPhpCodeSniffer
        phpcs -n --standard=PSR12 .
    elif [[ $optselected = 5 ]]; then
        # Faire les tests d'indentation du prof
        testFormatage


    elif [[ $optselected = 6 ]]; then
        # Effectuer le formatage de style PSR12
        checkPhpCodeSniffer
        phpcbf --standard=PSR12 .

    else
        # Rien donc on quitte le programme
        echo -e "Aucune option choisie.\nSortie..."
    fi

}

sousMenuChoixTests () {
    # Supprimer le fichier temporaire pour éviter que l'on garde l'ancien choix
    rm -f /tmp/initdevmegascript/menuselect.txt > /dev/null

    # Demander ce que l'utilisateur veut tester
    dialog --backtitle "Init. Au Dev. MegaScript" --checklist "Choisisez vos tests" 20 50 10 \
        1 "TD1" off \
        2 "TD2" off \
        3 "TD3" off \
        4 "TD4" off \
        5 "TD5" off \
        6 "TD6" off \
        7 "TD7" off \
        8 "TD8" off \
        9 "TD9" off \
        10 "TD10" off \
        11 "TD11" off \
        12 "421" off \

    2> /tmp/initdevmegascript/menuselect.txt
        #9 "TD5 - Principaux" off \
        #10 "TD5 - Complémentaires" off \
    
    # Effacer tout ce que dialog a affiché
    clear

    # Lire ce que l'utilisateur a choisi et le parser
    optselected=$(cat /tmp/initdevmegascript/menuselect.txt)
    opts=$(echo "$optselected" | tr " " "\n")

    # Faire les tests de tout ce que l'utilisateur a choisi
    echo "Exécution des tests sélectionnés..."
    for opt in $opts
    do
        case $opt in
            1)
                testTD1
            ;;
            2)
                testTD2
            ;;
            3)
                testTD3
            ;;
            4)
                testTD4
            ;;
            5)
                testTD5
            ;;
            6)
                testTD6
            ;;
            7)
                testTD7
            ;;
            8)
                testTD8
            ;;
            9)
                testTD9
            ;;
            10)
                testTD10
            ;;
            11)
                testTD11
            ;;
            12)
                test421
            ;;

        esac
    done
}

if [[ $DEBUG == 1 ]]; then
    echo "[DEBUG] Lancement du script..."
fi

# Installation de:
# - dialog pour faire les jolies interfaces
# - inotify-tools pour savoir quand nos scripts ont changés
echo "Vérification des dépendences..."
checkPackagesPrincipaux



# Créer le dossier temporaire nécessaire pour ce script
mkdir -p /tmp/initdevmegascript
mkdir -p /tmp/initdevmegascript/resultattests

# On ouvre l'interface
menuPrincipal
