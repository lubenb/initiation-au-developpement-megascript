# Todo List

## High Priority
- [ ] Fixer le téléchargement depuis le repo (pour les testeurs)
- [ ] Enlever le download dans le tmp et créer un vrai dossier
- [X] Continuer de trier les tests
- [X] Faire un trieur de tests (OPTIONNEL)
- [ ] Faire un fetcher automatique pour les nouveaux tests
- [ ] Faire une barre de chargement

## Medium Priority
- [X] Créer une fonction de nettoyage
- [ ] Optimiser le code
- [ ] Optimiser les caches / fichiers créés
- [ ] GREP les ECHECS pour le test d'indentations

## Low Priority
- [ ] Documenter le code
- [ ] Améliorer la phraséologie

Cette ToDo permet d'avoir une vision globale de la direction que prend le projet.