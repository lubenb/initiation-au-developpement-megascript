public class TesteurPourSommeTableau {

    public static void main(String[] args) {
        int T[] = new int[args.length - 1];
        int i;
        for (i = 0; i < args.length - 1; i++) {
            T[i] = Integer.parseInt(args[i]);
        }
        int rst = Integer.parseInt(args[args.length - 1]);

        if (Transition.sommeTableau(T) == rst) System.exit(0);//OK
        else System.exit(1);//ÉCHEC
    }

}