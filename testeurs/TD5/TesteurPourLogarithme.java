public class TesteurPourLogarithme {

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int rst = Integer.parseInt(args[2]);
        //System.out.println(Transition.logarithme(a,b));
        if (Transition.logarithme(a, b) == rst) System.exit(0);//OK
        else System.exit(1);//ÉCHEC
    }

}