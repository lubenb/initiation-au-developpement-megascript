import java.util.Arrays;

public class TesteurPourObtenirTrancheDeTableauEntre {

    public static void main(String[] args) {
        int T[] = new int[args.length - 2];
        int k;

        for (k = 0; k < args.length - 2; k++) {
            T[k] = Integer.parseInt(args[k]);
        }

        int i = Integer.parseInt(args[args.length - 2]);
        int j = Integer.parseInt(args[args.length - 1]);

        if (Arrays.equals(Transition.obtenirTrancheDeTableauEntre(T, i, j), Arrays.copyOfRange(T, i, j + 1)))
            System.exit(0);//OK
        else System.exit(1);//ÉCHEC
    }

}
