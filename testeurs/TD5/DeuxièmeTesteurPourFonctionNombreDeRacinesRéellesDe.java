public class DeuxièmeTesteurPourFonctionNombreDeRacinesRéellesDe {

    public static void main(String[] args) {
        if (Transition.fonctionNombreDeRacinesRéellesDe(1, 2, 3) == 0 && Transition.fonctionNombreDeRacinesRéellesDe(1, 2, 1) == 1 && Transition.fonctionNombreDeRacinesRéellesDe(1, 3, 1) == 2)
            System.exit(0);//OK
        else System.exit(1);//ÉCHEC
    }

}