public class TesteurPourProduitTableau {

    /**
     *
     */
    public static void main(final String[] args) {
        int[] tab = new int[args.length - 1];
        int i;
        for (i = 0; i < args.length - 1; i++) {
            tab[i] = Integer.parseInt(args[i]);
        }
        int rst = Integer.parseInt(args[args.length - 1]);

        if (Transition.produitTableau(tab) == rst) {
            System.exit(0); //OK
        } else {
            System.exit(1);
        } //ÉCHEC
    }
}