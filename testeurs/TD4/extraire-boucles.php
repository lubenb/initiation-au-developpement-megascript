<?php

function accoladeOuvrante(string $ligne): bool
{
    if (strpos($ligne, "{") === false) return false;
    else return true;
}

function accoladeFermante(string $ligne): bool
{
    if (strpos($ligne, "}") === false) return false;
    else return true;
}

$dansLeFichier = true;
while ($dansLeFichier == true) {
    do {
        $ligne = readline();
    } while ((strpos($ligne, "while (") === false and strpos($ligne, "for (") === false and strpos($ligne, "do {") === false) and ($ligne !== false));

    $i = 0;
    do {
        print ("$ligne\n");
        if (accoladeOuvrante($ligne)) $i++;

        if (accoladeFermante($ligne)) $i--;
        $ligne = readline();

    } while ($i > 0 and $ligne !== false);

    if ($ligne === false) $dansLeFichier = false;
}

print("---\n");