import java.util.Arrays;

public class TesteurPourCEstUnCarréMagiqueSagrada {

    public static void main(String[] args) {
        int[][] Sagrada = {{1, 14, 14, 4}, {11, 7, 6, 9}, {8, 10, 10, 5}, {13, 2, 3, 15}};
        int[][] Commun = {{1, 14, 14, 5}, {23, 7, 6, 9}, {8, 10, 10, 5}, {13, 2, 3, 15}};
        if (CarréMagique.cEstUnCarréMagique(Sagrada) && !CarréMagique.cEstUnCarréMagique(Commun)) {
            System.exit(0);// OK
        } else {
            System.exit(1);// ÉCHEC
        }
    }
}
