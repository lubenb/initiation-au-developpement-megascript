public class TesteurPourConstanteMagiqueDe {

    public static void main(String args[]) {
        int[][] CM = {{1, 14, 4, 15},
                {8, 11, 5, 10},
                {13, 2, 16, 3},
                {12, 7, 9, 6}};
        if (CarréMagique.constanteMagiqueDe(CM) == 34) {
            System.exit(0);
        } else {
            System.exit(1);
        }
    }
}
