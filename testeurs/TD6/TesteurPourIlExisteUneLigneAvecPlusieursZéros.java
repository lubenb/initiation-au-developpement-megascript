public class TesteurPourIlExisteUneLigneAvecPlusieursZéros extends IlExisteUneLigneAvecPlusieursZéros {

    public static void main(String[] args) {
        int[][] M = {{1, 2, 3}, {4, 0, 6}, {7, 0, 0}, {0, 2, 1}};
        int[][] N = {{0, 9, 2, 1}, {10, 5, 12, 34}, {4, 3, 0, 0}};
        int[][] P = {{0}};
        if (ilExisteUneLigneAvecPlusieursZéros(M) && ilExisteUneLigneAvecPlusieursZéros(N) && !ilExisteUneLigneAvecPlusieursZéros(P))
            System.exit(0);// OK
        else
            System.exit(1);// ÉCHEC
    }
}
