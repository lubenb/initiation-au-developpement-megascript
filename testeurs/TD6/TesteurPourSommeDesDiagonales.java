import java.util.Arrays;

public class TesteurPourSommeDesDiagonales {

    public static void main(String[] args) {
        int[][] M = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};

        int s = SommeDesDiagonales.sommeDesDiagonales(M);

        if (s == 68) {
            System.exit(0); // OK
        } else {
            System.exit(1); // ÉCHEC
        }
    }
}
