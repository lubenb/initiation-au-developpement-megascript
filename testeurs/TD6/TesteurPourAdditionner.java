import java.util.Arrays;

public class TesteurPourAdditionner{

    public static void main(String[] args){
	int[][] matP = {{1, 2, 3}, {4, 5, 6}};
	int[][] matQ = {{7, 8, 9}, {10, 11, 12}};
	int[][] matR = Additionner.additionner(matP, matQ);
	int[][] matS = {{8, 10, 12}, {14, 16, 18}};
 
	int[][] matPP = {{1, 2, 3, 4}, {5, 6, 7, 8}};
        int[][] matQQ = {{9, 10, 11, 12}, {13, 14, 15, 16}};
        int[][] matRR = Additionner.additionner(matPP, matQQ);
        int[][] matSS = {{10, 12, 14, 16}, {18, 20, 22, 24}};

        if (Arrays.deepEquals(matR, matS) && Arrays.deepEquals(matRR, matSS)) {
            System.exit(0); // OK
        } else {
            System.exit(1); // ÉCHEC
        }

    }
}
