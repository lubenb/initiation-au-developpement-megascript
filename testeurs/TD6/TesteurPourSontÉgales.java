import java.util.Arrays;

public class TesteurPourSontÉgales{

    public static void main(String[] args){
        int[][] P={{1,2,3},{4,5,6}};
        int[][] Q={{7,8,9},{10,11,12}};
	int[][] R={{1,2,3},{4,5,6}};
	int[][] S={{1,2},{3,4},{5,6}};
	int[][] T={{1,2,3},{4,4,5}};
	
        if (!SontÉgales.sontÉgales(P,Q) && SontÉgales.sontÉgales(P,R) && !SontÉgales.sontÉgales(P,S) && !SontÉgales.sontÉgales(S,P) && !SontÉgales.sontÉgales(P,T)){
            System.exit(0);//OK
        }
        else{
            System.exit(1);//ÉCHEC
        }
    }
    
}
