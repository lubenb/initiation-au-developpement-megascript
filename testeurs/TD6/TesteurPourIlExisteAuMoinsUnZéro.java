public class TesteurPourIlExisteAuMoinsUnZéro {

    public static void main(String args[]) {
        int[][] casPositif = new int[][]{{1, 2, 3}, {4, 0, 6}, {7, 8, 9}, {10, 11, 12}};
        int[][] casNégatif = new int[][]{{1, 2}, {4, 5}, {5, 6}};
        if (IlExisteAuMoinsUnZéro.ilExisteAuMoinsUnZéro(casPositif) && !IlExisteAuMoinsUnZéro.ilExisteAuMoinsUnZéro(casNégatif)) {
            System.exit(0);
        } else {
            System.exit(1);
        }
    }
}
