public class TesteurPourNombreDeZéros {

    public static void main(String[] args) {
        int[][] M = {{1, 2, 3}, {4, 0, 6}, {7, 0, 0}, {0, 2, 1}};
        int[][] N = {{0, 0, 2, 1}, {0, 0, 0, 0}, {4, 3, 0, 1}};
        int[][] O = {{1}};
        if (NombreDeZéros.nombreDeZéros(M) == 4 && NombreDeZéros.nombreDeZéros(N) == 7 && NombreDeZéros.nombreDeZéros(O) == 0)
            System.exit(0); // OK
        else
            System.exit(1); // ÉCHEC
    }
}
