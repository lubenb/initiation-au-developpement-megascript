import java.util.Arrays;

// tests avec les exemples donnés dans les spécifs :                                                                                                                                    
//     * - lancerDes([3,2,1], [true, false, true]) -> [4,2,1], ou [6,2,2], ou encore [3,2,1], etc.. (selon le résultat du lancer des dés 1 et 3)                                        
//     * - lancerDes([?,?,?], [true, true, true]) -> n'importe quelle figure (lancer des 3 dés)                                                                                         
public class TesteurPourLancerDes{

    public static void main(String argv[]){
        int[] résultat = new int[]{3,2,1};
        int déOK = résultat[1];
        Jeu421.lancerDes(résultat, new boolean[]{true, false, true});

        boolean test1 = 6 >= résultat[0] && résultat[0] >= résultat[1] && résultat[1] >= résultat[2] && résultat[2] >= 1 && (résultat[0] == déOK || résultat[1] == déOK || résultat[2] == déOK);

        résultat = new int[]{-1, -2, -3};
        Jeu421.lancerDes(résultat, new boolean[]{true, true, true});
        boolean test2 = 6 >= résultat[0] && résultat[0] >= résultat[1] && résultat[1] >= résultat[2] && résultat[2] >= 1 ;

        System.exit(test1 && test2 ? 0 : 1);
    }
}


