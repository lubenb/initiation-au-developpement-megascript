import java.util.Arrays;

public class TesteurPourGénérerFiguresEtPoints{

    public static void main(String args[]){
        int[][] figures = new int[56][3];
        int[] points = new int[56];
        Jeu421.genererFiguresEtPoints(figures, points);

        boolean test0 = Arrays.equals(figures[0], new int[]{2, 2, 1}) && points[0] == 1;
        boolean test1 = Arrays.equals(figures[1], new int[]{3, 2, 2}) && points[1] == 1;
        boolean test2 = Arrays.equals(figures[2], new int[]{3, 3, 1}) && points[2] == 1;
        // ...                                                                                                                                                                          
        boolean test53 = Arrays.equals(figures[53], new int[]{6, 6, 6}) && points[53] == 6;
        boolean test54 = Arrays.equals(figures[54], new int[]{1, 1, 1}) && points[54] == 7;
        boolean test55 = Arrays.equals(figures[55], new int[]{4, 2, 1}) && points[55] == 11;
        if (test0 && test1 && test2 && test53 && test54 && test55) System.exit(0); // OK                                                                                                
        else System.exit(1); // ÉCHEC                                                                                                                                                   
    }

}
