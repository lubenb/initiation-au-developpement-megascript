import java.util.Arrays;

public class TesteurPourJetChoisiParJoueur{

    public static void main(String[] argv){// à utiliser avec "1\n" en entrée                                                                                                           
        boolean jetHumain[] = Jeu421.jetChoisiParJoueur(true);// humain                                                                                                                 
        boolean jetOrdinateur[] = Jeu421.jetChoisiParJoueur(false);// ordinateur                                                                                                        
        boolean[] jetFalse={false, false, false};
        boolean[] jetHumainAttendu= {true, false, false};

        if (Arrays.equals(jetHumain, jetHumainAttendu) && !Arrays.equals(jetOrdinateur, jetFalse))
            System.exit(0);// OK                                                                                                                                                        
        else
            System.exit(1);// ÉCHEC                                                                                                                                                     
    }
}
