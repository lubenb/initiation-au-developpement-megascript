public class TesteurPourIndiceFigureTabFigures{

    public static void main(String[] args){
        int[][] tabFigures={{2,2,1},{5,3,2},{1,1,1},{1,2,4},{4,2,1}};
        int i = Jeu421.indiceFigureTabFigures(new int[]{2,2,1}, tabFigures);
        int j = Jeu421.indiceFigureTabFigures(new int[]{1,1,1}, tabFigures);
        int k = Jeu421.indiceFigureTabFigures(new int[]{4,2,1}, tabFigures);
        int l = Jeu421.indiceFigureTabFigures(new int[]{6,6,1}, tabFigures);
        if (i == 0 && j == 2 && k == 4 && l == -1) System.exit(0);
        else System.exit(1);
    }

}

