public class TesteurPourFigureEnChaîneDeCaractères{

    public static void main(String argv[]){
        int figure1[] = {4,2,1};
        int figure2[] = {6,5,4};

        String stringFigure1 = Jeu421.figureEnChaineDeCaracteres(figure1);
        String stringFigure2 = Jeu421.figureEnChaineDeCaracteres(figure2);

        if ((stringFigure1.equals("[4, 2, 1]") || stringFigure1.equals("[4,2,1]")) && (stringFigure2.equals("[6, 5, 4]") || stringFigure2.equals("[6,5,4]")))
            System.exit(0); // OK                                                                                                                                                       
        else
            System.exit(1); // ÉCHEC                                                                                                                                                    
    }

}
