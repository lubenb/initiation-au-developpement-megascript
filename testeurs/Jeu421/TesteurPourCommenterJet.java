/**                                                                                                                                                                                     
 * Donnée : un tableau de 3 booléens jet (dont un au moins est vrai)                                                                                                                    
 * Résultat : affiche les numéros de dés correspondant à true dans jet                                                                                                                  
 * Exemples :                                                                                                                                                                           
 * - commenterJet([false, true, false]) -> affiche "Le joueur lance le dé : 2"                                                                                                          
 * - commenterJet([true, false, true]) -> affiche "Le joueur lance les dés : 1 3"                                                                                                       
 */
public class TesteurPourCommenterJet{

    public static void main(String argv[]){
        boolean[] jet = new boolean[3];

        for (int i=0;i<3;i++) {
            if (argv[i].equals("true")) jet[i] = true;
            else jet[i] = false;
        }

        Jeu421.commenterJet(jet);
    }

}
