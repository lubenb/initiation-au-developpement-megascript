import java.util.Arrays ;

public class TesteurPourJetAleat{

    public static void main(String argv[]){
        int i = 0;
        int somme = 0;
        boolean tableauDeBooléens[] ;
        do {
            tableauDeBooléens = Jeu421.jetAleat();
            if (tableauDeBooléens[0]) somme += 1;
            if (tableauDeBooléens[1]) somme += 2;
            if (tableauDeBooléens[2]) somme += 4;
            // 1+2+3+4+5+6+7 = 28 / 7 == 4 (valeur moyenne d'un jet)                                                                                                                    
            i++ ;
        } while (! Arrays.equals(tableauDeBooléens, new boolean[] {false,false,false}) && i < 100);
        // si tout va bien, somme ne devrait pas être loin de 400...                                                                                                                    
        if (i == 100 && somme >= 300 && somme <= 500) System.exit(0);// OK                                                                                                              
        else System.exit(1);// ÉCHEC                                                                                                                                                    
    }
}

