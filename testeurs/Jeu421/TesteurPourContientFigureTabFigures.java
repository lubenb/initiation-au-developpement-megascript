public class TesteurPourContientFigureTabFigures{

    /**                                                                                                                                                                                 
     * Donnée : Une figure résultat et un tableau de figures tabFigures.                                                                                                                
     * Résultat : Retourne vrai si le tableau tabFigures contient la figure résultat et faux sinon.                                                                                     
     */

    /*                                                                                                                                                                                  
     * Une "figure" est un tableau de 3 entiers de 1 à 6 ordonné en ordre décroissant.                                                                                                  
     */

    public static void main(String args[]){
        int[][] tableauDeFigures = new int[][]{
            {6, 4, 2},
            {3, 3, 2},
            {5, 4, 3},
            {4, 2, 1},
            {1, 2, 2}
        };

        boolean test1 = Jeu421.contientFigureTabFigures(new int[]{6,4,2}, tableauDeFigures);
        boolean test2 = Jeu421.contientFigureTabFigures(new int[]{5,4,3}, tableauDeFigures);
        boolean test3 = Jeu421.contientFigureTabFigures(new int[]{1,2,2}, tableauDeFigures);
        boolean test4 = Jeu421.contientFigureTabFigures(new int[]{3,3,1}, tableauDeFigures);

        if (test1 && test2 && test3 && ! test4) System.exit(0); // OK                                                                                                                   
        else System.exit(1); // ÉCHEC                                                                                                                                                   
    }
}

