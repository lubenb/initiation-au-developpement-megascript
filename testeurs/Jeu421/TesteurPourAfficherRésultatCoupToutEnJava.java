import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**                                                                                                                                                                                     
 * Donnée : le résultat d'un coup et un numéro de coup                                                                                                                                  
 * Résultat : affiche le résultat obtenu ainsi que le numéro de coup.                                                                                                                   
 * Exemple : afficherResultatCoup([5,4,4], 2) -> affiche "Résultat du coup 2 : [5,4,4]"                                                                                                 
 */
public class TesteurPourAfficherRésultatCoupToutEnJava{

    public static void main(String argv[]){
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);

        int[] résultatCoup = {5,4,4};
        Jeu421.afficherResultatCoup(résultatCoup, 2);

        String output = outputStream.toString().trim();
        String sortieAttendue1 = "Résultat du coup 2 : [5, 4, 4]";
        String sortieAttendue2 = "Résultat du coup 2 : [5,4,4]";

        if (output.contains(sortieAttendue1) || output.contains(sortieAttendue2))
            System.exit(0);// OK                                                                                                                                                        
        else
            System.exit(1);// ÉCHEC                                                                                                                                                     
    }

}
