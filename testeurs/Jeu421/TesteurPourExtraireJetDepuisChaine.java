import java.util.Arrays;

public class TesteurPourExtraireJetDepuisChaine{

    public static void main(String args[]){
        boolean[] jet1 = Jeu421.extraireJetDepuisChaine("31");
        boolean[] jet2 = Jeu421.extraireJetDepuisChaine("123");
        boolean[] jet3 = Jeu421.extraireJetDepuisChaine(" 2   1  ");
        boolean test1 = Arrays.equals(jet1, new boolean[]{true, false, true});
        boolean test2 = Arrays.equals(jet2, new boolean[]{true, true, true});
        boolean test3 = Arrays.equals(jet3, new boolean[]{true, true, false});

        if (test1 && test2 && test3) System.exit(0); // OK                                                                                                                              
        else System.exit(1); // ÉCHEC                                                                                                                                                   
    }

}
