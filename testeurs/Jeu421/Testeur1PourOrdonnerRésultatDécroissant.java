import java.util.Arrays;

public class Testeur1PourOrdonnerRésultatDécroissant{

    public static void main(String args[]){
        int[] tableau1 = {1,2,3};
        Jeu421.ordonnerResultatDec(tableau1);
        var premierTest = Arrays.equals(tableau1,new int[]{3,2,1});

        int[] tableau2 = {1,6,1};
        Jeu421.ordonnerResultatDec(tableau2);
        var deuxièmeTest = Arrays.equals(tableau2, new int[]{6,1,1});

        int[] tableau3 = {6,5,4};
        Jeu421.ordonnerResultatDec(tableau3);
        var troisièmeTest = Arrays.equals(tableau3, new int[]{6,5,4});

        if (premierTest && deuxièmeTest && troisièmeTest) System.exit(0);
        else System.exit(1);
    }
}
