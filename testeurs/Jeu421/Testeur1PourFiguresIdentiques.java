public class Testeur1PourFiguresIdentiques{

    /**
     * Pré-requis : figure1 et figure2.sont des figures
     * Résultat : retourne vrai si les figures figure1 et figure2 sont identiques et faux sinon
     */
    public static void main(String argv[]){
        int[] fig1 = {5,2,1};
        int[] fig2 = {5,2,1};
        int[] fig3 = {6,5,3};

        boolean test1 = Jeu421.figuresIdentiques(fig1,fig2);
        boolean test2 = ! Jeu421.figuresIdentiques(fig2,fig3);

        System.exit(test1 && test2 ? 0 : 1);
    }

}



