import java.util.Arrays;

public class Testeur1PourÉchangerÉlémentsTableau{

    public static void main(String args[]){
        int[] tableau={1,2,3,4,5,6,7,8,9};
        Jeu421.echangerElementsTableau(tableau, 2, 5);
        System.exit(Arrays.equals(tableau,new int[]{1,2,6,4,5,3,7,8,9})?0:1);
    }
}

