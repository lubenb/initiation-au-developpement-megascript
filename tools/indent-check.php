<?php

$indentation = 0;
$n = 1;
$fichier = fopen($argv[1], "r");
$erreur = false;

while (($ligne = fgets($fichier)) != false and !$erreur) {
    $i = 0;

    while ($ligne[$i] == " ") {
        $i++;
    }

    if ($ligne[$i] == "\t") {
        echo "{$argv[1]} : erreur ligne $n : le caractère \\t (tabulation) est interdit (si la ligne doit être indentée alors utiliser des espaces à la place)\n";
        $erreur = true;
    } elseif ($ligne[$i] != "\n" && $i != $indentation && $ligne[$i] != "}" && $ligne[$i]!='*') {
        echo "{$argv[1]} : erreur ligne $n : $indentation espaces attendues / $i espaces trouvées \n";
        $erreur = true;
    } else {
        while ($i < strlen($ligne)) {
            if ($ligne[$i] == '{') {
                $indentation = $indentation + 4;
            }
            if ($ligne[$i] == "}") {
                $indentation -= 4;
            }
            $i++;
        }
    }

    $n++;
}

if (!$erreur) {
    echo "{$argv[1]} OK\n";
    exit(0);
} else {
    exit(1);// erreur
}
