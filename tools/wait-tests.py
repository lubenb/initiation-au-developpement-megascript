filesToCheck = [
    "/tmp/initdevmegascript/testsresult1.txt",
    "/tmp/initdevmegascript/testsresult2.txt",
    "/tmp/initdevmegascript/testsresult3.txt",
    "/tmp/initdevmegascript/testsresult4.txt",
]
for fn in filesToCheck:
    filesok = True
    with open(fn) as f:
        t = f.read().split("\n")
        if (t[-1] == ""):
            t.pop()
        if not (t[-1].startswith("-- Fin")):
            filesok = False

if filesok:
    open("/tmp/initdevmegascript/done","w")