def multi_split(string_to_split:str, splitters:list[str]):
    t = string_to_split
    for splitter in splitters:
            t = "&&&$&&&".join(t.split(splitter))
        
    return t.split("&&&$&&&")