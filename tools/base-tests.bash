#!/bin/bash

# Ce fichier n'est pas fait pour être executé seul, il doit être importé.

afficherLigneComplete () {
    longeurLigne=$(tput cols)
    longeurLigne=$(($longeurLigne-1))
    x=1
    while [ $x -le "$longeurLigne" ]; do
        echo -n "-"
        # shellcheck disable=SC2004
        x=$(( $x + 1 ))
    done
    echo ""
}
echo "Ceci est du debug, n'y faites pas attention"
