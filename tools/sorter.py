import re, sys
from typing import Literal
import funcs
file = sys.argv[1]

def read_bash_file(fp:str):
    with open(fp) as file:
        content = file.read()
    return content


def extract_function(
    lines: list[str], start: int, end: int
) -> dict[Literal["name"] | Literal["lines"], str | list[str]]:
    l = lines[start +1: end-1]
    #print(f"## DEBUG {lines[start+1]},{lines[end-1]}")
    if (lines[start].split(" ")[0][0:-1]=="testTD"):
        l.sort()
    return {"name": lines[start].split(" ")[0], "lines": l}


lines = read_bash_file(file).split("\n")
functions: dict[str, list[str]] = {}
inFunction = False
startFunction = 0
endFunction = 0
for i in range(len(lines)):
    line = lines[i]
    # print(line)
    if re.search("(){$", line) and not inFunction:
        # print("start")
        startFunction = i
        inFunction = True
    elif line.startswith("}") and inFunction:
        endFunction = i+1
        inFunction = False
        extracted_fn = extract_function(lines, startFunction, endFunction)
        if (extracted_fn["name"].startswith("test")):
            functions[extracted_fn["name"]] = extracted_fn["lines"]

for fnName in functions.keys():
    t = functions[fnName]
    i = 0
    while i < len(t):
        if (
            t[i] == ""
            or t[i] == "    "
            or "afficherLigneComplete" in t[i]
            or 'echo "--' in t[i]
        ):
            t.pop(i)
        else:
            i += 1

with open("./tools/base-tests.bash") as f2:
    t = f2.read()
    print(t)
for fnName in functions.keys():
    print(f"{fnName} () {{")
    print(f"    afficherLigneComplete\n    echo \"-- Scripts {fnName.split("test")[-1]}\"\n    afficherLigneComplete")
    for line in functions[fnName]:
        if funcs.multi_split(line,[".php",".java"])[0] != t:
            t = funcs.multi_split(line,[".php",".java"])[0]
            print("")
        print(line)
    print(f"\n    echo \"-- Fin Scripts {fnName.split("test")[-1]}\"\n}}\n")