#!/bin/bash
if [[ $(which inotifywait 2>&- | grep -qv ^/) ]]; then 
    if [[ -f /etc/debian-release ]]; then
        sudo apt-get install inotify-tools -y > /dev/null
    else
        echo "Vous devez installer inotify-tools"
        exit 1
    fi
fi

# Créer le dossier temporaire necessaire pour ce script
mkdir /tmp/initdevmegascript 2> /dev/null
cp ./tests-php.bash /tmp/initdevmegascript/tests-php.bash
cp ./tests-java.bash /tmp/initdevmegascript/tests-java.bash
cp ./tools/wait-tests.py /tmp/initdevmegascript/wait-tests.py

while inotifywait -e close_write tests-*; do
    echo "Updating files"
    cp ./tests-php.bash /tmp/initdevmegascript/tests-php.bash
    cp ./tests-java.bash /tmp/initdevmegascript/tests-java.bash
    cp ./tools/wait-tests.py /tmp/initdevmegascript/wait-tests.py
done