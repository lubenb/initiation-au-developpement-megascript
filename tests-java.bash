#!/bin/bash

# Ce fichier n'est pas fait pour être executé seul, il doit être importé.

afficherLigneComplete () {
    longeurLigne=$(tput cols)
    # shellcheck disable=SC2004
    longeurLigne=$(($longeurLigne-1))
    x=1
    while [ $x -le "$longeurLigne" ]; do
        echo -n "-"
        # shellcheck disable=SC2004
        x=$(( $x + 1 ))
    done
    echo ""
}
echo "Ceci est du debug, n'y faites pas attention"

# Définition des tests

telechargerTests () {
    printf "\n"
    echo "-- Scripts TelechargerTests"
    printf "\n"

    mkdir -p ./testeurs/java/Transition
    rm -r /tmp/initdevmegascript/repo
    mkdir -p /tmp/initdevmegascript/repo
    # shellcheck disable=SC2164
    cd /tmp/initdevmegascript/repo
    git clone https://gitlabinfo.iutmontp.univ-montp2.fr/lubenb/initiation-au-developpement-megascript/
    # shellcheck disable=SC2164
    # shellcheck disable=SC2103
    cd -
    rm -r ./testeurs/java/*
    cp -r /tmp/initdevmegascript/repo/initiation-au-developpement-megascript/testeurs/ ./testeurs/
    mv ./testeurs/testeurs/* ./testeurs/java
    echo "-- Fin Scripts TelechargerTests"
}

testTD5 () {
    printf "\n"
    echo "-- Scripts TD5"
    printf "\n"

    find . -name Transition.java -exec bash -c "grep -B 4 'public static int fonctionNombreDeRacinesRéellesDe' {} | grep Résultat | grep -e [rR]etourne -e [rE]envoie | grep nombre | grep racines | grep réelles | grep -q 'ax² + bx + c' && echo {} OK || echo {} ÉCHEC" \; | sort
    # find . -name Transition.java -exec bash -c "grep -H -B 3 procédureNombreDeRacinesRéellesDe '{}' | grep -e Donnée -e Résultat" \;
    # find . -name Transition.java -exec bash -c "javac -d . {} && (javap Transition | grep -q -F 'public static int logarithme(int, int);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac -d . {} && (javap Transition | grep -q -F 'public static int produitTableau(int[]);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac -d . {} && (javap Transition | grep -q -F 'public static int puissance(int, int);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac -d . {} && (javap Transition | grep -q -F 'public static int[] obtenirTrancheDeTableauEntre(int[], int, int);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac testeurs/java/TD5/DeuxièmeTesteurPourFonctionNombreDeRacinesRéellesDe.java {} -d ~/tmp && java -cp ~/tmp DeuxièmeTesteurPourFonctionNombreDeRacinesRéellesDe && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac testeurs/java/TD5/TesteurPourLogarithme.java {} -d ~/tmp && timeout 1 java -cp ~/tmp TesteurPourLogarithme 82 3 4 && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac testeurs/java/TD5/TesteurPourLogarithme.java {} -d ~/tmp && timeout 1 java -cp ~/tmp TesteurPourLogarithme 82 3 4 && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac testeurs/java/TD5/TesteurPourObtenirTrancheDeTableauEntre.java {} -d ~/tmp && java -cp ~/tmp TesteurPourObtenirTrancheDeTableauEntre 1 2 5 7 3 4 1 3 && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac testeurs/java/TD5/TesteurPourProcédureNombreDeRacinesRéellesDe.java {} -d . && java TesteurPourProcédureNombreDeRacinesRéellesDe | tr -d '\n' | grep -q 012 && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac testeurs/java/TD5/TesteurPourProduitTableau.java {} -d ~/tmp && (java -cp ~/tmp TesteurPourProduitTableau 1 && echo {} OK || echo {} ÉCHEC) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac testeurs/java/TD5/TesteurPourPuissance.java {} -d ~/tmp && java -cp ~/tmp TesteurPourPuissance 5 0 1 && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac testeurs/java/TD5/TesteurPourPuissance.java {} -d ~/tmp && java -cp ~/tmp TesteurPourPuissance 5 8 390625 && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac testeurs/java/TD5/TesteurPourSommeTableau.java {} -d ~/tmp && java -cp ~/tmp TesteurPourSommeTableau 1 -2 3 6 5 -1 12 && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "javac testeurs/java/TD5/TesteurPourSommeTableau.java {} -d ~/tmp && java -cp ~/tmp TesteurPourSommeTableau 1 -2 3 6 5 -1 12 && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort | grep OK
    find . -name Transition.java -exec bash -c "java MethodExtractor '{}' 'public static int puissance' | grep -c -e 'if (' -e 'else if (' -e 'else {' | grep -q 4 && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name Transition.java -exec bash -c "grep -B 3 sommeTableau '{}' | grep Donnée | grep tableau | grep -q entiers && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name Transition.java -exec bash -c "javac testeurs/java/Transition/TesteurPourObtenirTrancheDeTableauEntre.java '{}' -d . && java -cp . TesteurPourObtenirTrancheDeTableauEntre 1 2 5 7 3 4 1 3 && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Transition.java -exec bash -c "grep -A 8 'int produitTableau' '{}' | grep -q 'while *(.*&&.*)' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name Transition.java -exec bash -c "grep -B 3 procédureNombreDeRacinesRéellesDe '{}' | grep Donnée | grep -v entrée | grep -e 'non nul' -e '!= 0' -e 'différent de 0' -e 'coefficients.*quadratique' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name Transition.java -exec bash -c "grep -H -B 3 'public static int fonctionNombreDeRacinesRéellesDe' '{}' | grep Résultat | grep -i -e 'retourne' -e 'renvoie'" \;

    printf "\n"
    echo "-- Fin Scripts TD5"
    printf "\n"
}

testTD6 () {
    printf "\n"
    echo "-- Scripts TD6"
    printf "\n"

    find . -name Additionner.java -exec bash -c "javac -d . {} && (javap Additionner | grep -q -F 'public static int[][] additionner(int[][], int[][]);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Additionner.java -exec bash -c 'javac -d . testeurs/java/TD6/TesteurPourAdditionner.java {} && (java TesteurPourAdditionner {} && echo {} OK || echo {} ÉCHEC) || echo {} ERREUR COMPIL.' \; | sort

    find . -name IlExisteAuMoinsUnZéro.java -exec bash -c "javac -d . testeurs/java/TD6/TesteurPourIlExisteAuMoinsUnZéro.java {} && java TesteurPourIlExisteAuMoinsUnZéro && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name IlExisteAuMoinsUnZéro.java -exec bash -c "javac -d . {} && (javap IlExisteAuMoinsUnZéro | grep -q -F 'public static boolean ilExisteAuMoinsUnZéro(int[][]);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name IlExisteAuMoinsUnZéro.java -exec bash -c "grep -q 'while *(' {} && ! grep -q 'for *(' {} && echo {} OK || echo {} ÉCHEC" \; | sort

    find . -name IlExisteUneLigneAvecPlusieursZéros.java -exec bash -c "javac -d . {} && (javap IlExisteUneLigneAvecPlusieursZéros | grep -q -F 'public static boolean ilExisteUneLigneAvecPlusieursZéros(int[][]);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; | sort
    find . -name IlExisteUneLigneAvecPlusieursZéros.java -exec bash -c "javac testeurs/java/TD6/TesteurPourIlExisteUneLigneAvecPlusieursZéros.java {} -d ~/tmp && (java -cp ~/tmp TesteurPourIlExisteUneLigneAvecPlusieursZéros && echo {} OK || echo {} ÉCHEC) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name IlExisteUneLigneAvecPlusieursZéros.java -exec bash -c "php outils/extraire-boucles.php < {} | grep -q -e break -e return && echo {} ÉCHEC || echo {} OK" \; | sort

    find . -name LigneAvecUnMaxDeZéros.java -exec bash -c "javac -d . testeurs/java/TD6/TesteurPourLigneAvecUnMaxDeZéros.java {} && timeout 1 java TesteurPourLigneAvecUnMaxDeZéros && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name LigneAvecUnMaxDeZéros.java -exec bash -c "javac -d . {} && (javap LigneAvecUnMaxDeZéros | grep -q -F 'public static int ligneAvecUnMaxDeZéros(int[][]);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name LigneAvecUnMaxDeZéros.java -exec bash -c "javac -d . testeurs/java/matrices/TesteurPourLigneAvecUnMaxDeZéros.java {} && timeout 1 java TesteurPourLigneAvecUnMaxDeZéros && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort

    find . -name NombreDeZéros.java -exec bash -c "javac -d . {} && (javap NombreDeZéros | grep -q -F 'public static int nombreDeZéros(int[][]);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name NombreDeZéros.java -exec bash -c "javac -d ~/tmp {} testeurs/java/TD6/TesteurPourNombreDeZéros.java && java -cp ~/tmp TesteurPourNombreDeZéros && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name NombreDeZéros.java -exec bash -c "javac -d ~/tmp {} testeurs/java/matrices/TesteurPourNombreDeZéros.java && java -cp ~/tmp TesteurPourNombreDeZéros && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort | grep OK

    find . -name SommeDesDiagonales.java -exec bash -c 'javac -d ~/tmp testeurs/java/TD6/TesteurPourSommeDesDiagonales.java {} && (java -cp ~/tmp TesteurPourSommeDesDiagonales {} >/dev/null && echo {} OK || echo {} ÉCHEC) || echo {} ERREUR COMPIL.' \; 2>/dev/null | sort

    find . -name SontÉgales.java -exec bash -c "javac -d . testeurs/java/TD6/TesteurPourSontÉgales.java {} && java TesteurPourSontÉgales && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name SontÉgales.java -exec bash -c "javac -d . {} && (javap SontÉgales | grep -q -F 'public static boolean sontÉgales(int[][], int[][]);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; | sort
    find . -name SontÉgales.java -exec bash -c "php outils/extraire-boucles.php < {} | (! grep -q -e 'return' -e 'break') && echo {} OK || echo {} ÉCHEC" \; | sort

    printf "\n"
    echo "-- Fin Scripts TD6"
    printf "\n"
}

testTD7 () {
    printf "\n"
    echo "-- Scripts TD7"
    printf "\n"

    find . -name CarréMagique.java -exec bash -c "javac -d . {} && (javap CarréMagique | grep -q -F 'public static boolean cEstUnCarréMagique(int[][]);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name CarréMagique.java -exec bash -c "javac -d . {} && (javap CarréMagique | grep -q -F 'public static int constanteMagiqueDe(int[][]);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name CarréMagique.java -exec bash -c "javac -d . {} && (javap CarréMagique | grep -q -F 'public static void afficherCarré(int[][]);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name CarréMagique.java -exec bash -c "javac -d . {} && (javap CarréMagique | grep -q -F 'public static void remplirEnCarréMagique(int[][]);' && echo {} OK || echo {} MÉTHODE INTROUVABLE) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name CarréMagique.java -exec bash -c "javac testeurs/java/TD7/TesteurPourCEstUnCarréMagiqueSagrada.java {} -d ~/tmp && (java -cp ~/tmp TesteurPourCEstUnCarréMagiqueSagrada >/dev/null && echo {} OK || echo {} ÉCHEC) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name CarréMagique.java -exec bash -c "test -s {} && javac -cp ~/tmp testeurs/java/TD7/TesteurPourConstanteMagiqueDe.java {} -d ~/tmp && (java -cp ~/tmp TesteurPourConstanteMagiqueDe && echo {} OK || echo {} ÉCHEC) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name CarréMagique.java -exec bash -c "checkstyle -c outils/jp-checks.xml '{}' 1>/dev/null 2>/dev/null && echo {} OK || echo {} ÉCHEC" \; | sort

    printf "\n"
    echo "-- Fin Scripts TD7"
    printf "\n"
}

testTD8 () {
    printf "\n"
    echo "-- Scripts TD8"
    printf "\n"

    printf "\n"
    echo "-- Fin Scripts TD8"
    printf "\n"
}

testTD9 () {
    printf "\n"
    echo "-- Scripts TD9"
    printf "\n"

    find . -name NumérationRomaine.java -exec bash -c "javac {} -d /tmp && javap -cp /tmp NumérationRomaine | grep -q 'public static java.lang.String unités(int);' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name NumérationRomaine.java -exec bash -c "javac {} -d /tmp && javap -cp /tmp NumérationRomaine | grep -q 'public static java.lang.String dizaines(int);' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name NumérationRomaine.java -exec bash -c "javac {} -d /tmp && javap -cp /tmp NumérationRomaine | grep -q 'public static java.lang.String centaines(int);' && echo {} OK || echo {} ÉCHEC" \; | sort
    find . -name NumérationRomaine.java -exec bash -c "javac {} -d /tmp && javap -cp /tmp NumérationRomaine | grep -q 'public static void afficher(int);' && echo {} OK || echo {} ÉCHEC" \; | sort


    printf "\n"
    echo "-- Fin Scripts TD9"
    printf "\n"
}

testTD10 () {
    printf "\n"
    echo "-- Scripts TD10"
    printf "\n"

    find . -name Fraction.java -exec bash -c "javac {} -d . && javap Fraction | grep -q 'public void réduire();' && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Fraction.java -exec bash -c "javac -cp ~/tmp {} -d ~/tmp && (javap -cp ~/tmp Fraction | grep -q 'public Fraction fractionRéduite()' && echo {} OK || echo {} MÉTHODE ABS) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Fraction.java -exec bash -c "javac -cp ~/tmp {} Ut.java -d . && (javap -cp . Fraction | grep -q 'public Fraction fois(Fraction)' && echo {} OK || echo {} MÉTHODE ABS) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Fraction.java -exec bash -c "javac '{}' Ut.java -d . && (javap -cp . Fraction | grep -q 'public Fraction plus(Fraction)' && echo {} OK || echo {} MÉTHODE ABS) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Fraction.java -exec bash -c "javac '{}' Ut.java -d . && (javap -cp . Fraction | grep -q 'public Fraction puissance(int)' && echo {} OK || echo {} MÉTHODE ABS) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Fraction.java -exec bash -c "javac '{}' Ut.java -d . && (javap -cp . Fraction | grep -q 'public boolean égale(Fraction)' && echo {} OK || echo {} MÉTHODE ABS) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Fraction.java -exec bash -c "javac '{}' Ut.java -d . && (javap -cp . Fraction | grep -q 'public boolean estIrréductible();' && echo {} OK || echo {} MÉTHODE ABS) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort

    printf "\n"
    echo "-- Fin Scripts TD10"
    printf "\n"
}

testTD11 () {
    printf "\n"
    echo "-- Scripts TD11"
    printf "\n"

    find . -name Date.java -exec bash -c "javac -cp ~/tmp {} -d . && (javap -cp . Date | grep -q 'public Date(int, int, int);' && echo {} OK || echo {} ÉCHEC) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Date.java -exec bash -c "javac -cp ~/tmp {} -d . && javap -cp . Date | grep -q 'public java.lang.String toString();' && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort

    printf "\n"
    echo "-- Fin Scripts TD11"
    printf "\n"
}

test421 () {
    printf "\n"
    echo "-- Scripts Jeu421"
    printf "\n"

    find . -name Jeu421.java -exec bash -c "javac  {} src/Ut.java -d /tmp && java -cp /tmp Testeur1PourFiguresIdentiques && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac testeurs/java/Jeu421/Testeur1PourFiguresIdentiques.java {} src/Ut.java -d /tmp && java -cp /tmp Testeur1PourFiguresIdentiques && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac testeurs/java/Jeu421/Testeur1PourLancerDes.java {} src/Ut.java -d /tmp && java -cp /tmp Testeur1PourLancerDes && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac testeurs/java/Jeu421/Testeur1PourOrdonnerRésultatDécroissant.java {} src/Ut.java -d /tmp && java -cp /tmp Testeur1PourOrdonnerRésultatDécroissant && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac -d /tmp testeurs/java/Jeu421/Testeur1PourÉchangerÉlémentsTableau.java {} src/Ut.java && java -cp /tmp Testeur1PourÉchangerÉlémentsTableau && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac Testeur1PourLancerDes.java {} -d /tmp && java -cp /tmp Testeur1PourLancerDes && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac Testeur1PourFiguresIdentiques.java {} -d /tmp && java -cp /tmp Testeur1PourFiguresIdentiques && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac {} TesteurPourIndiceFigureTabFigures.java {} -d /tmp || (echo {} ERREUR COMPIL. && exit 1) && (java -cp /tmp TesteurPourIndiceFigureTabFigures && echo {} OK || echo {} ÉCHEC)" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourContientFigureTabFigures.java {} -d /tmp && java -cp /tmp TesteurPourContientFigureTabFigures.java >/dev/null && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac -d /tmp TesteurPourGénérerFiguresEtPoints.java {} || (echo {} ERREUR COMPIL. && false) && (java -cp /tmp TesteurPourGénérerFiguresEtPoints >/dev/null && echo {} OK || echo {} ÉCHEC)" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourBoolAleat.java {} -d . && java -cp . TesteurPourBoolAleat && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourBoolAleat.java {} -d . && java -cp . TesteurPourBoolAleat && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourJetAleat.java {} -d . && java -cp . TesteurPourJetAleat && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourChaineRelanceValide.java {} -d /tmp && java -cp /tmp TesteurPourChaineRelanceValide '   ' | grep -q 'Votre réponse doit comporter au moins un chiffre de 1 à 3.' && java -cp /tmp TesteurPourChaineRelanceValide ' 31 3' | grep -q 'Il y a une répétition du chiffre 3.' && java -cp /tmp TesteurPourChaineRelanceValide '421' | grep -q -e '4 n.est pas un chiffre de 1 à 3.' && echo {} OK || echo {} ÉCHEC " \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourExtraireJetDepuisChaine.java {} -d . && java TesteurPourExtraireJetDepuisChaine -cp . && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourJetChoisiParHumain.java {} Ut.java -d /tmp && echo -e '4\n13\n1 3\n' | java -cp /tmp TesteurPourJetChoisiParHumain 2 | grep -cF '[true, false, true]' | grep -q '^2$' && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourJetChoisiParJoueur.java {} Ut.java -d . && echo -e '1\n' | java -cp . TesteurPourJetChoisiParJoueur 1>/dev/null && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourFigureEnChaîneDeCaractères.java {} Ut.java -d . && java -cp . TesteurPourFigureEnChaîneDeCaractères && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourLancerDes.java {} Ut.java -d . && (java -cp . TesteurPourLancerDes && echo {} OK || echo {} ÉCHEC) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourAfficherRésultatCoupToutEnJava.java {} Ut.java -d . && (java -cp . TesteurPourAfficherRésultatCoupToutEnJava && echo {} OK || echo {} ÉCHEC) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourCommenterJet.java {} Ut.java -d . && java -cp . TesteurPourCommenterJet false true false | grep -q 'Le joueur lance le dé : 2' && java -cp . TesteurPourCommenterJet true false true | grep -q 'Le joueur lance les dés : 1 3' && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourJouerCoup.java Ut.java {} -d . && timeout 1 java -cp . TesteurPourJouerCoup && echo {} OK || echo {} ÉCHEC" \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac -d . Testeur1PourÉchangerÉlémentsTableau.java {} Ut.java && (java -cp . Testeur1PourÉchangerÉlémentsTableau && echo {} OK || echo {} ÉCHEC) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort
    find . -name Jeu421.java -exec bash -c "javac TesteurPourPremierChoix.java {} Ut.java -d . && (echo -e 'peut-être\noui\n' | timeout 1 java -cp . TesteurPourPremierChoix 'recommencer ?' oui non | grep -q true && echo {} OK || echo {} ÉCHEC) || echo {} ERREUR COMPIL." \; 2>/dev/null | sort

    printf "\n"
    echo "-- Fin Scripts Jeu421"
    printf "\n"
}
